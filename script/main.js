//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//																														//
//		Name: Nigromance main functions for HTML% Paint																	//
// 		Version: 1.0.0 																									//
// 		Author: Nigromance																								//
// 		Date: June of 2014																								//
//																														//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////
//													//
//				Browser Checking					//
//													//
//////////////////////////////////////////////////////
var navName = window.navigator.appName;
var navVers = window.navigator.appVersion;
	var IEVers = 9;
	var indIEVers = navVers.indexOf('MSIE');
	if ( navName === 'Microsoft Internet Explorer' ){
		listVers = navVers.substr(indIEVers+5).split(';');
		IEVers = parseInt(listVers[0]);
	}

if ( (navName === 'Microsoft Internet Explorer' && IEVers < 10) || navName === 'Netscape' && parseInt(navVers) < 5 ){
	window.location.href="http://www.nigromance.com/too_old.html";
}

/************************************************************************/

//////////////////////////////////////////////////////
//													//
//				Cross-Browser Functions				//
//													//
//////////////////////////////////////////////////////
function preventDefault(e){
	var e = e || window.event;
	if ( e.preventDefault ){
		e.preventDefault();
	}
	if ( e.stopPropagation ){
		e.stopPropagation();
	}
	return false;
}

function addEvent(elemento,evento,funcion,bubble){

	if (elemento.attachEvent){
    	elemento.attachEvent('on'+evento,funcion);
		//window.event.cancelBubble = !bubble;
    	return true;
  	}else  if (elemento.addEventListener){
    	elemento.addEventListener(evento,funcion,bubble);
      	return true;
    }else{
      return false;
	}
}

function trigger(e){
	if(window.event){
		elemento = window.event.srcElement;
	}else if(e){
		elemento = e.target;
	}
	return elemento;
}

function mouseCoord(e){
	var e = e || window.event;

	var pageX = e.pageX;
	var pageY = e.pageY;
	if (pageX === undefined) {
    	pageX = e.clientX + document.body.scrollLeft + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
    	pageY = e.clientY + document.body.scrollTop + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
	}
	return {x:pageX,y:pageY};
}  

function scrollAbsolute(element){
	var scrollTop = 0,scrollLeft = 0,left = 0,top = 0;
	var doc = document.documentElement;
	var body = document.body;
	var elem = canvas;
	scrollLeft -= (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
	scrollTop -= (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

	return{left:scrollLeft,top:scrollTop};
} 

function coordIntoCanvas(canvas,e){
	var raton = mouseCoord(e);
	var cb = canvas.getBoundingClientRect();
	var scroll = scrollAbsolute(canvas);

	return {x: Math.round( (raton.x-cb.left+scroll.left) * canvas.width/cb.width), y: Math.round( (raton.y-cb.top+scroll.top) * canvas.height/cb.height )};
}

function coordIntoElement(element,e){
	var raton = mouseCoord(e);
	var cb = element.getBoundingClientRect();
	var scroll = scrollAbsolute(element);

	return {x: Math.round( (raton.x-cb.left+scroll.left) ), y: Math.round( (raton.y-cb.top+scroll.top) )};
}

function rightBtn(e){
	var e = e || window.event;
	var val = false;

	if ( e === undefined || e === null ){
		val = false;
	}else{
		
		if (e.which)
			val = (e.which == 3);
		else if (e.button) 
			val = (e.button == 2);
	}
	return val;
}

function get_firstchild(element){
	var fChild = element.firstChild;
	while (fChild.nodeType!=1){
		fChild=fChild.nextSibling;
	}
	return fChild;
}

function viewPortDim(){
	var height = ( window.innerHeight || window.document.documentElement.clientHeight );
	var width = (  window.innerWidth || window.document.documentElement.clientWidth );
	return {height:height,width:width};		
}
/************************************************************************/