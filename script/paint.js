//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//																														//
//		Name: HTML5 Paint ( Win98 Vers )																				//
// 		Version: 1.0.0 (Beta)																							//
// 		Author: Nigromance																								//
// 		Date: June of 2014																								//
//		Thanks: To developers who sharing their wisdom make this possible, saving me a lot of time of life!!			//
//				* William Malone | http://www.williammalone.com/articles/html5-canvas-javascript-paint-bucket-tool/		//		  
//				* Paul Lewis | http://www.html5rocks.com/en/tutorials/canvas/hidpi/										//
//				* Garry Tan | http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c 						//
//																														//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
                       M,..    .                                                
                     .NMMMMMMMMMMN.        ..., ..                              
                    ..MMMMMMMMMMMMMM  .,MNMMMMMMMM                              
                    ,MMMMMMMMMMMMMMMM$   . MMMMMMMMM.                           
                   .MMMMMMMMMMMMMO...7D.    .DMMMMMMMM=                         
                   .MMMMMMMNN .               .MMMMMMMMMM .                     
               .:.  +MMMN.      ..=ZDMMMM7      7MMMMMMMMMM.                    
            ..NMMZ.  MM.         .?MMMMMMMMO.   ..MMMMMMMMMM                    
            7MMMMM  .M   . MNI..   MMMMZ   .   .   MMMMMMMMMM.                  
           .MMMMMM~.     ZMMMMMM  .MMMM. ...  M:.  ZMMMMMMMMMM                  
           MMMMMMMM.     MMMMMMM.   MMMO=MN   MM8   MMMMMMMMMMM.                
           MMMMMMMM    .MMMMMMMM. ,  NMMM8  .MMMM:  .MMMMMMMMMI                 
         .~MMMMMMMMD   MMMMMMMMM. .        .ZMMMMM. .MMMMMMMM.                  
        . MMMMMMMMMM.    MMMMMMM. .M ... .OMMMMMMM.  .MMMMMM                    
         8MMMMMMMMMMM.    ?MMMMM  .MMMMMMMMMMMMMMZ    MMN.                      
         . MMMMMMMMMMM.   .$MMMM: .MMMMMMMMMMMMMM.   ...                        
           .8MMMMMMMMMM     ,MMMD  .MMMMMMMMMMM$.                               
            ..OMMMMMMMMM.   . MMM   .MMMMMMMMO..  .MMMMMM+.                     
               ..NMMMMMMM.    .,M?.  .......    7MMMMMMMMMMM.                   
                   .. 7DMM       ..         . MMMMMMMMMMMMMMM.                  
                          .             ..MMMMMMMMMMMMMMMMMMM=                  
                DMMMZ           ..  ?MMMMMMMMMMMMMMMMMMMMMMMM                   
                MMMMMMMN.       ?MMMMMMMMMMMMMMMMMMMMMMMMMMMM.                  
                MMMMMMMMMM8.       .,MMMMMMMMMMMMMMMMMMMMMMM .                  
                IMMMMMMMMMMMMM .      . ..:MMMMMMMMMMMMMMMM                     
                 ZMMMMMMMMMMMMMMM?...       ..+MMMMMMMMMMD.                     
                 ..MMMMMMMMMMMMMMMMMMMMMD,.    .OMMMMMMN .                      
                   .MMMMMMMMMMMMMMMMMMMMMI.      ,MMM .                         
                     ~MMMMMMMMMMMMMMMMMMD.       .,.                            
                       MMMMMMMMMMMMMMMM                                         
                       ~MMMMMMM7, .                                             
                        MM .                                                    
                        .  								by Nigromance.

*/
//////////////////////////////////////////////////////////////////////
//																	//
//						 GLOBAL VARIABLES							//
//																	//
//////////////////////////////////////////////////////////////////////
var filename = 'untitled';												// Creating Image & File name
var dragImg = new Image();												// Auxiliar Image Object for drag Image case
var loadImg = new Image();												// Auxiliar Image Object for loading images
var url = window.URL || window.webkitURL;								// URL object for file data processing
var sounds = {chord:new Audio(), error: new Audio()};					// Applications sounds to launch
var savedImagesData = {ind:-1,images:[]};								// Array with the saved imageDatas of canvas
var canvasImageData={},offscreenImageData={};							// Canvas image datas used for temporal storing data in processing
var canvas,ctx,auxCanvas,auxCanvasCtx,offscreenCanvas,offscreenCtx;		// Canvas and context objects
var clipBoard = {};														// Obiously... the clipboard for copied elements.
var titlesInfo=[														// Info showed for every tool selected
	{title:'Free-Form Select',phrase:'Selects a free-from part of the picture to move, copy, or edit.'},
	{title:'Select',phrase:'Selects a rectangular part of the picture to move, copy, or edit.'},
	{title:'Eraser/Color Eraser',phrase:'Erases a portion of the picture, using the selected eraser shape.'},
	{title:'Fill With Color',phrase:'Fills an area with the current drawing color.'},
	{title:'Pick Color',phrase:'Picks up a color from the picture for drawing.'},
	{title:'Magnifier',phrase:'Changes the magnification.'},
	{title:'Pencil',phrase:'Draws a free-form line one pixel wide.'},
	{title:'Brush',phrase:'Draws using a brush with the selected shape and size.'},
	{title:'Airbrush',phrase:'Draws using an airbrush of the selected size.'},
	{title:'Text',phrase:'Inserts text into the picture.'},
	{title:'Line',phrase:'Draws a straight line with the selected line width.'},
	{title:'Curve',phrase:'Draws a curved line with the selected line width.'},
	{title:'Rectangle',phrase:'Draws a rectangle with the selected fill style.'},
	{title:'Polygon',phrase:'Draws a polygon with the selected fill style.'},
	{title:'Ellipse',phrase:'Draws an ellipse with the selected fill style.'},
	{title:'Rounded Rectangle',phrase:'Draws a rounded rectangle with the selected fill style.'}
]
var keyboard = {								// Store what command key is pressed
				ctrl:false,
				alt:false,
				shift:false
};
var canvasMouse = {x:0,							// Mouse coord over Canvas (x,y).
					y:0,	
					cursor:'c_default',			// CSS class for cursor image
					keypress:false,				// Mouse key pressed
					right:false,				// Mouse right button cliked
					inside:false,				// Mouse over Canvas.
					cursor:"c_default"			// Current cursor class
};	
	
//////////////////////////////////////////////////////////////////////
//																	//
//						 TOOLS VARIABLES							//
//																	//
//////////////////////////////////////////////////////////////////////	

		//////////////////////////////////////////////////////////////////////////////////////
		//	OBJECT - tools 																	//
		//		Store every data needed for the tools working, including objects with the 	//
		//		neccesary methods.															//
		//////////////////////////////////////////////////////////////////////////////////////  	
		var tools = {types: ['free-form_select','select','eraser','fill_with_color','pick_color','magnifier','pencil','brush','airbrush','text','line','curve','rectangle','polygon','ellipse','rounded_rectangle'],
			selected:"",						// ID of the tool selected.
			drawing:false,						// Boolean which tell us if user is drawing something.
			localClipboardImg:{					// Local clipboard for the copied/cutted image. Store an imageData object.
					imageData:{},
					cutted:{x:0,y:0,width:0,height:0}
			},
			initX:0,							// Initial coords of mouse when click over the canvas.
			initY:0,				
			primColor:"rgb(0,0,0,255)",			// Primary color selected
			secColor:"rgb(255,255,255,255)",	// Secondary color selected
			eraserDim:4,						// Dimension of the eraser square in pixels
			brushType:2,						// What type of the 12 brush types is selected
			airbrushRad:4.5,					// Radius of circle inside which sprite is drawn
			airbrushInterv:null,				// Drawing interval for the airbrush
			lineWidth:1,						// Line width selected
			roundRad:6,							// Radius of the rounded rentangle corners
			ctrlPntX:Array(0,0),				// Control points for bezier curves (x coordinate)
			ctrlPntY:Array(0,0),				// Control points for bezier curves (y coordinate)
			anchPntX:0,							// Control point for bezier curves (x coordinate)
			anchPntY:0,							// Control point for bezier curves (y coordinate)
			curveState:0,						// Drawing Bezier curve state (0,1,2)
			filling: false,						// True if flood fill algorithm is running
			rectangle:{stroke:true,fill:false},	// Fill & stroke option selected for the rectangle tool,
			circle:{stroke:true,fill:false},	// circle tool,
			rounded:{stroke:true,fill:false},	// and rounded rectangle tool. Tells if it have to fill and/or stroke.
			backgroundTransp:true,				// Background transparent for text o area selection
			changeBackgroundTransp: function(){	// Change "opaque" parameter for the select,free-form select and text tools.
												// This object update the "draw opaque" option in the image submenu too.
				var menu_row = document.getElementById('image_opaque');
				var cell = get_firstchild(menu_row);
				if ( this.backgroundTransp ){	// HIDE
					cell.className = '';
					this.backgroundTransp = false;
				}else{						//SHOW
					cell.className = 'check';
					this.backgroundTransp = true;
				}
				
			},
			clearImage:{						// Tool for the clearing image command.
				able: true,
				menuElement:null,
				changeState: function(state){
					if ( state ){
						this.able = true;
						this.menuElement.className = '';
					}else{
						this.able = false;
						this.menuElement.className = 'disabled';
					}
				},
				clear:function(){
					if ( this.able ){
						window.ctx.fillStyle = window.tools.secColor;
						window.ctx.fillRect(0,0,canvas.width,canvas.height);
						saveImage();
					}
				}
			},
			magnifier:{							// Magnifier tool
				is: false,							// Is or Is not Zoom applied
				zoomMag:1,							// Magnification of zoom (X1 X2 X4 X8)
				currentMag:2,						// Magnification to apply
				appSize:{width:0,height:0},			// App windows size where canvas is.
				zoomWin:{w:0,wm:0,h:0,hm:0},
				lastTool:'pencil',					// Previous selected tool
				calcZoomWin: function(){			// Make the maths for canvas box size with the current zoom size selected.
					this.zoomWin.w = Math.round(this.appSize.width/this.currentMag);
					this.zoomWin.wm = Math.round(this.zoomWin.w/2);
					this.zoomWin.h = Math.round(this.appSize.height/this.currentMag);
					this.zoomWin.hm = Math.round(this.zoomWin.h/2);
				},
				zoomIn: function(){					// Zoom in action. This affects to the canvas box size not its pixel number. 
													//				   Then come back to the previous tools.
					this.is = true;				
					var xp = canvasMouse.x*tools.magnifier.currentMag;
					var yp = canvasMouse.y*tools.magnifier.currentMag;
					var wp = canvas.width*tools.magnifier.currentMag;
					var hp = canvas.height*tools.magnifier.currentMag;
					this.zoomMag = this.currentMag;

					window.canvasChangeBoxSize(wp,hp);
					document.getElementById('canvas_container').scrollTop = yp - this.appSize.height/2;
					document.getElementById('canvas_container').scrollLeft = xp - this.appSize.width/2;
					canvasMouse.keypress = false;

					if ( this.lastTool === 'text' ){
						this.lastTool = 'pencil';
					}
					window.selectTool(this.lastTool);
				},
				zoomOut: function(){				// Come back to normal zoom magnification. Then come back to the previous tool
					this.is = false;
					this.zoomMag = 1;
					window.canvasChangeBoxSize(canvas.width,canvas.height);
					canvasMouse.keypress = false;
					if ( this.lastTool === 'text' ){
						this.lastTool = 'pencil';
					}
					window.selectTool(this.lastTool);
				}
			},
			poligon: {							// --POLIGON-- tool management object
						stroke:true,				// Have or not have to stroke
						fill:false,					// Have or not have to fill
						points:[],					//Vertex coordinates of the poligon which user is painting	
						draw: function(callback){	//Stroke the poligon

								var points = this.points;
								var numPoints = points.length;

								// If there are two points at least
							  	if ( numPoints > 1 ){
							  		for ( var i=1;i<numPoints;i++ ){
							  			drawTrayectory(points[i-1].x,points[i-1].y,points[i].x,points[i].y,callback);
									}
								}
						},
						close: function(){			//Close the poligon from the current point to the initial one.
							var points = this.points;
							var haveToStroke = this.stroke;
							var haveToFill = this.fill;
							var strokeWidth = (haveToFill && ! haveToStroke) ? 2 : tools.lineWidth;
							var fillColor = canvasMouse.right*!haveToStroke + !canvasMouse.right*haveToStroke ? tools.secColor : tools.primColor;
							
							//Drawing the definitive poligon
							
								// Clean both canvas	
							ctx.putImageData(canvasImageData,0,0);
							offscreenCtx.clearRect(0,0,canvas.width,canvas.height);

							// In Fill case 
							if ( haveToFill ){

								offscreenCtx.beginPath();
								offscreenCtx.moveTo(points[0].x,points[0].y);
								for ( var i=1,end=points.length;i<end;i++ ){
									offscreenCtx.lineTo(points[i].x,points[i].y);
								}
								offscreenCtx.closePath();
								offscreenCtx .fillStyle = fillColor;
								offscreenCtx.fill();
								drawClean(127);
							}

							// Color for the stroke
							if ( haveToFill && !haveToStroke ){
								ctx.fillStyle = fillColor;
							}else{
								ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
							}

							// Special tratement for the 1px lines with inside fill
							if (strokeWidth === 1 && haveToFill && haveToStroke){
								offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
								offscreenCtx.beginPath();
								offscreenCtx.moveTo(points[0].x,points[0].y);
								for ( var i=1,end=points.length;i<end;i++ ){
									offscreenCtx.lineTo(points[i].x,points[i].y);
								}
								offscreenCtx.closePath();

								if ( haveToFill && !haveToStroke ){
									offscreenCtx.fillStyle = fillColor;
								}else{
									offscreenCtx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
								}
								offscreenCtx.stroke();
								drawClean(127);
							}else{

									// Draw strokes
								tools.poligon.draw(function(x,y){tools.poligon.fillPixel(x,y,window.ctx);});
									//Close Path
								drawTrayectory(points[0].x,points[0].y,points[points.length-1].x,points[points.length-1].y,function(x,y){tools.poligon.fillPixel(x,y,window.ctx);});
							}

								//Update CanvasImageData
							canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
								//Reset Poligon
							this.reset();

							saveImage();
						},
						reset: function(){			//Back to nothing	
							this.points.length = 0;
							ctx.putImageData(canvasImageData,0,0);
						},
						fillPixel: function(x,y,cntx){
							var size = (this.fill && !this.stroke) ? 2 : tools.lineWidth;
							var half = size === 1 ? 0 : size/2;

							cntx.fillRect(x-half,y-half,size,size);
						}
				}					
		};		


		//////////////////////////////////////////////////////////////////////////////////////
		//	OBJECT - text 																	//
		//		Store every data needed for the text tool, including objects with the 		//
		//		neccesary methods to do so.													//
		////////////////////////////////////////////////////////////////////////////////////// 
		var text = {
			active:false,							// Boolean which tells if the text box is working/focus or not.
			coord:{X:0,Y:0},						// First line text coordinates.
			content:'',								// String with all the text written by the user.
			contentMemo:{content:'',abs:0},			// Something like clipboard used for copy/cut/paste/back operations.
			contentSelected:{						// Object to handle the potion text selection by the user mouse.
					from:(0,0),					
					to:(0,0),
					clicked:false,
					textSelected:false,
					reset: function(){
						this.from = this.to = (0,0);
						this.clicked = this.textSelected = false;
					}
			},
			written:[''],							// Array of strings. Is a 2D matrix built from "content" string and the text box width.
			cursor:{								// Object to handle the cursor position inside the content string and written matrix.
					active:false,
					line:0,
					chr:0,
					abs:0,
					hide: false,
					run: function(){
						if ( window.text.active ){
							this.hide = !this.hide;
							window.draw();
							window.setTimeout(function(){window.text.cursor.run();},500);
						}
					}
			},
			container:{X:0,Y:0,W:0,H:0},
			ENTER:String.fromCharCode(13),
			calculateBox: function(){				// Make the maths for the text box size
				var selFrame = window.selectionFrame;
				var fontSize = window.textTool.size.selected;
				selFrame.x = this.coord.X-1;
				selFrame.y = this.coord.Y-fontSize*1.5;
				selFrame.width = selFrame.width !== 0 ? selFrame.width : 50;
				this.update();
			},
			activate: function(){					// Do the neccesary actions to the text tools activation
				var selFrame = window.selectionFrame;
				var fontSize = window.textTool.size.selected;
				window.textTool.activate();
				this.active = true;
				selFrame.active = true;
				this.cursor.run();
				selFrame.height = fontSize*1.67+2;
				this.calculateBox();
			},
			deactivate: function(){					// Do the neccesary actions to the text tools deactivation
				window.textTool.deactivate();
				ctx.putImageData(canvasImageData,0,0);
				this.drawText();
				this.reset();
				window.selectionFrame.active = false;
				window.selectionFrame.width = 0;
				window.selectionFrame.height = 0;
			},
			contentBack: function(){				// Go back a char write step (Step back command)
				if ( this.contentMemo.content.length > 0 ){
					var cAux = this.content;
					var aAux = this.cursor.abs;
					this.content = this.contentMemo.content;
					this.cursor.abs = this.contentMemo.abs;
					this.contentMemo.content = cAux;
					this.contentMemo.abs = aAux;
					this.update();
					this.draw();
				}
			},
			reset: function(){						// RESET of the neccesary objects and variables to the original state.
				this.active = false;
				this.written.length = 0;
				this.written.push('');
				this.content = '';
				this.cursor.abs = 0;
				this.cursor.line=0;
				this.cursor.chr=0;
				this.contentSelected.reset();
			},
			drawText: function(){					// Draw the text of written matriz into the canvas inside the text box limits.
				var written = this.written;
				var lines = written.length;
				var charSize = window.textTool.size.selected;
				var lineHeight = charSize*1.67;
				var cursor = this.cursor;
				var underline = window.textTool.style.underline;
				var selFrame = window.selectionFrame;

				//BACKGROUND
				if ( tools.backgroundTransp ){
					ctx.fillStyle = tools.secColor;
					ctx.fillRect(selFrame.x,selFrame.y,selFrame.width,selFrame.height+0.5);
				}
				// TEXT
				if ( lines > 0 ){
					var lineY = 0;
					textString='';
					for ( var line=0;line<lines; line++){
						lineY = this.coord.Y+(lineHeight*line);
						textString = written[line];
						ctx.fillStyle = tools.primColor;
						ctx.fillText(textString,this.coord.X,lineY);
						// UNDERLINE - making a line under the text
						if ( underline ){
							ctx.fillRect(this.coord.X,lineY+2,ctx.measureText(textString).width,2);
						}
					}
				}
			},
			drawSelected: function(){				// Draw a blue background behind the text for every selected char
				var written = this.written;
				var lines = written.length;
				var charSize = window.textTool.size.selected;
				var lineHeight = charSize*1.67;
				var contSel = this.contentSelected;
				
					if (  contSel.textSelected ){
						var lineY = 0;
						var localY = lineHeight*0.83;
						from = contSel.from;
						to = contSel.to;

						if ( from[0] > to[0] || (from[0] === to[0] && from[1] > to[1]) ){
							from = contSel.to;
							to = contSel.from;
						}

						for ( var line=from[0],end = to[0]+1; line<end; line++){
							lineY = this.coord.Y+(lineHeight*line);
							
							fromChr = ( line === from[0] ) ? from[1] : 0;
							fromX = ctx.measureText(written[line].substr(0,fromChr)).width;

							toChr = ( line === to[0] ) ? to[1] : this.written[line].length;
							toX = ctx.measureText(written[line].substr(0,toChr)).width;

							ctx.fillStyle='rgb(0,0,168)';
							ctx.fillRect(this.coord.X + fromX,lineY - localY,toX-fromX,lineHeight);

							ctx.fillStyle = '#FFFFFF';
							ctx.fillText(this.written[line].substring(fromChr,toChr),this.coord.X + fromX,lineY);
						}
					}
			},
			draw: function(){						// Text tool main draw function. From this one the others are handle and called.
				var written = this.written;
				var lines = written.length;
				var charSize = window.textTool.size.selected;
				var lineHeight = charSize*1.67;
				var cursor = this.cursor;
				var underline = window.textTool.style.underline;
				var selFrame = window.selectionFrame;

				ctx.save();
					// Clipping REGION
					ctx.beginPath();
					ctx.rect(selFrame.x,selFrame.y,selFrame.width,selFrame.height);
					ctx.clip();

					this.drawText();	// Backgroung (filled or not) and text
					this.drawSelected();
					
					// CURSOR
					if ( !cursor.hide && cursor.active ){
						var curY = this.coord.Y+(cursor.line*lineHeight)-lineHeight*0.83; 
						var lineString = this.written[cursor.line]; 
						var curX = this.coord.X;

						if ( lineString.length < cursor.chr ){
							cursor.chr = lineString.length;
						}

						if ( cursor.chr === lineString.length ){
							curX += ctx.measureText(lineString).width;
						}else{
							curX += ctx.measureText(lineString.substr(0,cursor.chr)).width;
						}
						
						if ( !tools.backgroundTransp ){
							offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
							offscreenCtx.fillStyle = '#000000';
							offscreenCtx.fillRect(curX,curY,2,lineHeight);
							
							window.drawInverseColor();
							ctx.drawImage(offscreenCanvas,0,0);
							offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
						}else{
							ctx.fillStyle = window.inverseColor(tools.secColor);
							ctx.fillRect(curX,curY,2,lineHeight);
						}
					}
				ctx.restore();
				// SELECTION FRAME
				window.selectionFrame.draw();
				
			},
			coordToAbs: function(line,chr){			// Transform the cursor coordinates inside the written matrix (line, char)
													// 			to linear position inside the content string variable.
				var count = 0;
				var wttn = this.written;
				for ( var i=0; i<line; i++ ){
					count += wttn[i].length;
				}
				return count + chr;
			},
			deleteChar: function(pos){				// Delete a char in the "pos" linear coordinate of the content string
				var content = this.content;
				var contSel = this.contentSelected;
				//Save content
					this.contentMemo.content = this.content;
					this.contentMemo.abs = this.cursor.abs;
				// IF THERE IS A TEXT SELECTION
				if (  contSel.textSelected ){
					var from = this.coordToAbs(contSel.from[0],contSel.from[1]);
					var to = this.coordToAbs(contSel.to[0],contSel.to[1]);
					var moveCursor = to-from;

					if ( from > to ){
						var fromA = from;
						from = to;
						to = fromA;
						moveCursor = 0;
					}
					contSel.reset();
					this.cursor.abs -= moveCursor;
					this.content = content.substring(0,from) + content.substring(to);
				// IF NOT DELETE A CHAR
				}else{
					this.content = content.substring(0,pos)+content.substr(pos+1);
				}
				this.update();
			},
			pasteText: function(stringToPaste){		// Paste a string into the cursor position. Delete the selected text if it is.
				//Save content
					this.contentMemo.content = this.content;
					this.contentMemo.abs = this.cursor.abs;
				// IF THERE IS A TEXT SELECTION
				if (  this.contentSelected.textSelected ){
					this.deleteChar();
				}
				this.content = this.content.substring(0,this.cursor.abs) + stringToPaste + this.content.substr(this.cursor.abs);
				this.cursor.abs += stringToPaste.length;
				this.update();
			},
			deleteKey: function(){					// Action when the user push the "delete key"
				if ( this.content.length > this.cursor.abs ){
					this.deleteChar(this.cursor.abs);
				}
			},
			addChar: function(keycode){				// Add character handler. Is the main keyboard handler for this tool.
				var numWords = this.written.length;
				var cursor = this.cursor;
				var written = this.written;
				var content = this.content;

				switch (keycode){
					case 8: 	//BACKSPACE
							if ( cursor.abs > 0 ){
								this.deleteChar(cursor.abs-1);
								cursor.abs--;
								this.cursorCoordUpdt();
							}
						break;	
					case 37: // CURSOR LEFT
							if ( cursor.abs > 0 ){
								cursor.abs--;
							}
							this.cursorCoordUpdt();
						break;	
					case 38: // CURSOR UP
							if ( cursor.line > 0 ){
								cursor.line--;
								var leng = written[cursor.line].length;
								if ( leng < cursor.chr){
									cursor.chr = leng;
								}
							}
							this.cursorAbsUpdt();
						break;
					case 39: // CURSOR RIGHT
							if (content.length > cursor.abs){
								cursor.abs++;
							}
							this.cursorCoordUpdt();
						break;
					case 40: // CURSOR DOWN
							if ( written[cursor.line+1] !== undefined ){
								cursor.line++;
								var leng = written[cursor.line].length;
								if ( leng < cursor.chr){
									cursor.chr = leng;
								}
							}
							this.cursorAbsUpdt();
						break;

					default:
						var fontSize = window.textTool.size.selected;
						var minHeight = window.selectionFrame.height + 1.67*fontSize+2;						
						var maxWidth = window.ctx.measureText(this.written[this.written.length-1]).width;
						//Save content
							this.contentMemo.content = this.content;
							this.contentMemo.abs = this.cursor.abs;

						if (  this.contentSelected.textSelected ){
							this.deleteChar();
						}
						
							if ( (selectionFrame.y + window.selectionFrame.height) < canvas.height){

								var newChar = String.fromCharCode(keycode);
								this.content = this.content.substring(0,cursor.abs) + newChar + this.content.substr(cursor.abs);
								cursor.abs++;

								//ENTER - Divide into to phrases inserting a new line into "written" array
								if ( keycode === 13 ){	
									this.written.splice(cursor.line+1,0,this.written[cursor.line].substr(cursor.chr));
									this.written[cursor.line] = this.written[cursor.line].substr(0,cursor.chr);
								}
								this.update();
								
							}else{
								window.sounds.error.play();
							}
				}
				draw();
			},
			mouseToTextCoord: function(x,y){				// From a x,y coordinates inside the canvas, this method put the cursor
															// into the right written matrix coordinates (line,char).
				var frame = window.selectionFrame;
				var cursor = this.cursor;
				var coordX = frame.x;
				var coordY = frame.y;
				var lineHeight =  window.textTool.size.selected*1.67;
				if ( x >= coordX+2 && y >= coordY+2 ){
					var written = this.written;
					var lines = written.length;
					// Getting line
					for (var i=0; i<lines; i++){
						if ( coordY+(lineHeight*i) < y && coordY+(lineHeight*(i+1)) >= y ){
							cursor.line = i;
							break;
						}
					}
 					// Getting Char
 					var lineChars = written[cursor.line];
 					var lineLeng = lineChars.length;

 					if ( window.ctx.measureText(lineChars).width+coordX <= x){
 						cursor.chr = lineLeng;
 					}else{
 						var leng,lengNext,pointValue;
	 					for( var i=0,end=lineChars.length; i<end ; i++ ){
	 						leng = window.ctx.measureText(written[cursor.line].substring(0,i)).width;
	 						lengNext = window.ctx.measureText(written[cursor.line].substring(0,i+1)).width;
	 						pointValue = (lengNext - leng)/2 + leng;

	 						if ( pointValue+coordX > x){
	 							cursor.chr = i;
	 							break;
	 						}
	 					}
 					}
					return [cursor.line,cursor.chr];
				}
			},
			selectAll: function(){							// Select all written text
				var lastLine = this.written.length-1;
				this.contentSelected.textSelected = true;
				this.contentSelected.from = [0,0];
				this.contentSelected.to = [lastLine,this.written[lastLine].length];
				this.cursor.abs = this.content.length;
				this.cursorCoordUpdt();
			},
			selectTextMove:function(x,y){					// Update the selected text while the user move the clicked mouse into the text box
				var textbox = window.selectionFrame;
				if ( x > textbox.x+2 && x < textbox.x+textbox.width-2 && y > textbox.y+2 && y < textbox.y+textbox.height-2 ){
					this.contentSelected.to = this.mouseToTextCoord(x,y);
					this.contentSelected.textSelected = true;
					this.cursorAbsUpdt();
				}
			},
			clickOnText: function(x,y){						// Detect a click into the text box and put the curso in the right place.
				var coords = this.mouseToTextCoord(x,y);

				if ( window.keyboard.shift ){
					this.selectTextMove(x,y);
				}else{
					this.contentSelected.from = coords;
					this.contentSelected.to = coords;
					this.contentSelected.clicked = true;
					this.contentSelected.textSelected = false;
					this.cursorAbsUpdt();
				}
				
			},
			cursorAbsUpdt: function(){						// Update the linar coordinates of the cursor from the matrix coordinates.
				var written = this.written;
				var cursor = this.cursor;
				var absCount = 0;

				for ( var i=0,end=cursor.line; i<end; i++ ){
					absCount += written[i].length;
				}
				cursor.abs = absCount + cursor.chr;
			},
			cursorCoordUpdt: function(){					// Update the matrix coordinates of the cursor from the linear coordinates.
				var written = this.written;
				var cursor = this.cursor;
				var absValue = cursor.abs;
				var charCount = 0;
				var line = 0;

				for ( var i=0,end=written.length; i<end; i++ ){
					if ( charCount + written[i].length >= absValue ){
						line=i;
						break;
					}else{
						charCount += written[i].length;
						line=i;
					}
				}
				cursor.line = line;
				cursor.chr = absValue - charCount;
				if ( written[line][cursor.chr-1] === this.ENTER ){
					cursor.line++;
					cursor.chr = 0;
				}
			},
			updateCoords: function(){					// Update the coordinates of the selectionFrame text box from the family and size font
				var selFrame = window.selectionFrame;
				this.coord.X = selFrame.x + 1;
				this.coord.Y = selFrame.y +  window.textTool.size.selected*1.5;
				this.update();
			},
			update: function(){							// Main update method. This one make all maths to build the "written" matrix from the "content" string.
				var content = this.content;
				var written = [''];
				var line = 0;
				var width = window.selectionFrame.width-8;
				var height = window.selectionFrame.height-2;
				var paragraphs = content.split(this.ENTER);
				for (var p=0,endp=paragraphs.length; p<endp; p++){
					var words = paragraphs[p].split(' ');
					if (p>0){
						written[line] += this.ENTER;
						line++;
						written.push('');
					}
					for ( var w=0,endw=words.length; w<endw; w++){
						var wordLeng = window.ctx.measureText(words[w]).width;
						var lineLeng = window.ctx.measureText(written[line]+'').width;
						var space = width - lineLeng;
						if ( w > 0 ){
							written[line] += ' ';
						}
						//Not enought space for the word
						if (  wordLeng >= space ){	
							var word = words[w];
							// Moving to the next line
							line++;
							written.push('');
							// But the word is bigger than box width
							if ( wordLeng >= width ) {
								var remainLeng = wordLeng; // How many length is remaining to fix
								
								var L = 0;
								do{
									L = 1;
									while(  window.ctx.measureText(word.substr(0,L)).width < width ){
										L++;
									}
									L--;										
									// Breaking the word in two
									remainLeng -= window.ctx.measureText(word.substr(0,L)).width;
									written[line] += word.substr(0,L);
									word = word.substr(L);
									line++;
									written.push('');
								}while(remainLeng >= width);	
							}
							written[line] = word;
						}else{
							written[line] += words[w];
						}
					}
				}
				
				var selFrame = window.selectionFrame;
				var fontSize = window.textTool.size.selected;
				var minHeight = written.length*1.67*fontSize+2;
				if ( selFrame.height < minHeight ){
					selFrame.height= minHeight;
				}
				this.written = written;
				this.cursorCoordUpdt();
			}
		};

		//////////////////////////////////////////////////////////////////////////////////////
		//	OBJECT - selectionFrame															//
		//		This object handle the frame box for select, fre-form select and text tool.	//
		//		Is the most important, and maybe the largest, object into the app.			//
		//		Reset, de/activate, resize, copy, paste, cut, rotate, skew, stretch...		//
		//		the image inside this frame. 												//
		////////////////////////////////////////////////////////////////////////////////////// 
		var selectionFrame={
			active:false,								// Tells us the frame state
			width:0,									// Frame dimensions
			height:0,
			x:0,y:0,									// Frame position
			rotateAng:0,								// Angle to rotate
			translateX:0,								// Translation for transform drawing
			translateY:0,
			drag:false,									// Boolean to know when the frame is dragging
			sizing:false,								// Boolean to know when the frame is resizing
			moving:false,								// Boolean to know when the frame is moving
			hover:false,								// Boolean to know when the mouse is over the frame
			pointSize:3,								// Size, in pixels, of the resizing blocks around the frame.
			bDraw: false,								// Boolean to know when the user is sizing the frame at the begining (select tool)
			cutZone:false,								// Boolean to know if user is cutting a form (free-form select tool)
			cutted: {x:0,y:0,width:0,height:0,is:false,	// Object which store the information about the image cutted from the canvas when the frame is created.
					imageData:{},
					imageDataNoFill:{},
					backgroundColor:'',
					manualCoords:[],
					maxX:0,maxY:0,minX:0,minY:0
			},
			pointOver:"none",							// Store which point is mouse over, if it is.
			resizePoints:[{p:'n',x:0,y:0},				// Resize points name and position
							{p:'ne',x:0,y:0},
							{p:'e',x:0,y:0},
							{p:'se',x:0,y:0},
							{p:'s',x:0,y:0},
							{p:'sw',x:0,y:0},
							{p:'w',x:0,y:0},
							{p:'nw',x:0,y:0}],
			isOver: function (px,py){					// Return true if the user mouse is inside the frame, false in other case.
				var x = this.x,y = this.y;
				var width = this.width,height = this.height;
				
				if(this.width<0){			
					width = -this.width;
					x -= width;
				}
				if(this.height<0){			
					height = -this.height;
					y -= height;
				}	
				
				ctx.beginPath();
				ctx.rect(x,y,width,height);
				return ctx.isPointInPath(px,py);
			},
			isOverBorder: function(px,py){				// Return true is the user mouse is over the frame border or false in other case.
				var x = this.x-4,y = this.y-4;
				var width = this.width+8,height = this.height+8;

				ctx.beginPath();
				ctx.rect(x,y,width,4);
				ctx.rect(x+width-4,y,4,height);
				ctx.rect(x,y+height-4,width,4);
				ctx.rect(x,y,4,height);
				
				return ctx.isPointInPath(px,py);
			},
			isOverPntSelec: function (px,py){			// Return true is the user mouse is over any resize pointer or false in other case.
				var pointMatch = false;
				
				for ( i=0; i<this.resizePoints.length ; i++){
					ctx.beginPath();
					ctx.rect(this.resizePoints[i].x,this.resizePoints[i].y,this.pointSize,this.pointSize);
					if(ctx.isPointInPath(px,py)){
						pointMatch = this.resizePoints[i].p;	
					}
				}
				return pointMatch;
			},
			dragImage: function(){						// Function used when an image is dragged into the canvas from the user computer
														// 		Put the dragged image inside the frame as a cutted image.
				var width = window.dragImg.width;
				var height = window.dragImg.height;
				var self = window.selectionFrame;

					if ( window.tools.selected !== 1 )
						window.selectTool('select');

					if ( self.active )
						self.deactivate();

					self.active = true;
					self.x = self.y = self.cutted.x = self.cutted.y = 0;
					self.width = self.cutted.width = width;
					self.height = self.cutted.height = height;
					self.cutted.is = true;

					offscreenCanvas.width = width;
					offscreenCanvas.height = height;

					offscreenCtx.clearRect(0,0,width,height);
					offscreenCtx.drawImage(window.dragImg,0,0,width,height);

					self.cutted.imageData = offscreenCtx.getImageData(0,0,width,height);
					self.cutted.imageDataNoFill = offscreenCtx.getImageData(0,0,width,height);

					offscreenCtx.clearRect(0,0,offscreenCanvas.width,offscreenCanvas.height);

					window.dragImg.src = '';

					self.cutted.backgroundColor = window.tools.secColor;
					self.buildNoFillImage();

					document.getElementById('app_blocker').style.display = 'none';
			},
			selectAll:function(){					// Select all canvas as a cutted image
				if ( window.tools.selected !== 1 )
					window.selectTool('select');

				if ( this.active )
					this.deactivate();

				this.active = true;
				this.x = this.y = this.cutted.x = this.cutted.y = 0;
				this.width = this.cutted.width = canvas.width;
				this.height = this.cutted.height = canvas.height;
				this.cutted.is = true;

				this.cutted.imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
				this.cutted.imageDataNoFill = ctx.getImageData(0,0,canvas.width,canvas.height);

				ctx.fillStyle = tools.secColor;
				ctx.fillRect(0,0,canvas.width,canvas.height);

				window.canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);

				this.cutted.backgroundColor = window.tools.secColor;
				this.buildNoFillImage();
			},	
			initCutting: function(x,y){				// Reset the conditions to start an image cutting
				this.active = true;
				this.cutZone = true;
				this.cutted.maxX = this.cutted.minX = x;
				this.cutted.maxY = this.cutted.minY = y;
				offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
			},				
			manualCut: function(x,y){				// Method to update the current free-form cutting
				//Clipping coords
				if ( x < 0 ) x = 0;
				if ( x > canvas.width ) x = canvas.width;
				if ( y < 0 ) y = 0;
				if ( y > canvas.height ) y = canvas.height;
				//Check max & min
				if ( this.cutted.maxX < x )  this.cutted.maxX = x;
				if ( this.cutted.minX > x )  this.cutted.minX = x;
				if ( this.cutted.maxY < y )  this.cutted.maxY = y;
				if ( this.cutted.minY > y )  this.cutted.minY = y;
				//Store Coords
				this.cutted.manualCoords.push([x,y]);
				//Draw painted line
				window.drawTrayectory(tools.initX,tools.initY,x,y,
					function(x,y){
						window.offscreenCtx.fillStyle = '#000000';
						window.offscreenCtx.fillRect(x,y,1,1);
					}
				);
				window.drawInverseColor();
				updateDimBox(this.cutted.maxX - this.cutted.minX,this.cutted.maxY - this.cutted.minY);
			},
			stretchSkew: function(skH,skV,stH,stV){		// Strech & skew transform for the cutted image inside the frame.
					var self = window.selectionFrame;
					var zoom = window.tools.magnifier.zoomMag;
					var hrad = Math.PI*skH/180;
					var vrad = Math.PI*skV/180;
					var skewh = hrad === 0 ? 0 : Math.tan(hrad);
					var skewv = vrad === 0 ? 0 : Math.tan(vrad);
					// DIMENSION RUSULT OF CANVAS
						var newWidth = Math.round((self.width*stH/100 + Math.abs(skewh*self.height)));
						var newHeight = Math.round((self.height*stV/100 + Math.abs(skewv*self.width)));
					// DIMESION UPDATING
						self.width = newWidth;
						self.height = newHeight;
						var newX = 0;
						var newY = 0;
						if ( skewh < 0 ){
							newX -= Math.round(skewh*self.height);
							newY -= Math.round(Math.atan(skewv)*newX);
						}
						if ( skewv < 0 ){							
							newY -= Math.round(skewv*self.width);
							newX -= Math.round(Math.atan(skewh)*(newY));
						}
					
					// Clean the main canvas
					ctx.clearRect(0,0,canvas.width,canvas.height);
					window.offscreenCanvas.width = canvas.width;
					window.offscreenCanvas.height = canvas.height;
					window.offscreenCtx.putImageData(this.cutted.imageData,0,0);
				// DRAW THE NEW IMAGE
					window.ctx.fillStyle = window.tools.secColor;
					window.ctx.fillRect(0,0,newWidth,newHeight);
					ctx.save();
						window.ctx.transform(stH/100,skewv,skewh,stV/100,0,0);
						window.ctx.drawImage(offscreenCanvas,this.cutted.x,this.cutted.y,this.cutted.width,this.cutted.height,newX,newY,this.cutted.width,this.cutted.height);
					ctx.restore();
					
					//Take the drawn image from main canvas to offsetCanvas
						this.cutted.imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
						this.cutted.imageDataNoFill = ctx.getImageData(0,0,canvas.width,canvas.height);
					// Recovering the previous image into the main canvas
						ctx.putImageData(canvasImageData,0,0);
					// Updating cutted parameters
						this.cutted.backgroundColor = window.tools.secColor;
						this.buildNoFillImage();

						this.cutted.width = this.width;
						this.cutted.height = this.height;
						this.cutted.x = 0;
						this.cutted.y = 0;
					
				// Draw 
					this.draw();
			},
			rotateImage:function(ang){			// Rotationg transformation of the cutted image inside the frame
				// DOING THE MATHS
					this.rotateAng = ang;
				//Change dimensions
					if ( ang%(Math.PI) ){ 
						var width = this.width;
						this.width = this.height;
						this.height = width;
					}

					this.translateX = 0;
					this.translateY = 0;
					var W = this.width;
					var H = this.height;

					if ( this.rotateAng === Math.PI/2 ){
						this.translateX = this.x+this.y+this.width;
						this.translateY = this.y-this.x;
						W = this.height;
						H = this.width;
					}
					if ( this.rotateAng === Math.PI ){
						this.translateX = 2*this.x+this.width;
						this.translateY = 2*this.y+this.height;
					}
					if ( this.rotateAng === Math.PI*3/2){
						this.translateX = this.x-this.y;
						this.translateY = this.y+this.x+this.height;
						W = this.height;
						H = this.width;
					}
				// Clean the main canvas
					ctx.clearRect(0,0,canvas.width,canvas.height);
					window.offscreenCanvas.width = canvas.width;
					window.offscreenCanvas.height = canvas.height;
					window.offscreenCtx.putImageData(this.cutted.imageData,0,0);
				// Put the rotated image on it
					ctx.save();
						ctx.translate(this.translateX,this.translateY);
						ctx.rotate(this.rotateAng);
						ctx.drawImage(offscreenCanvas,this.cutted.x,this.cutted.y,this.cutted.width,this.cutted.height,this.x,this.y,W,H);
					ctx.restore();
				
					this.updateCutted();
				// Draw 
					this.draw();
			},
			updateCutted: function(){								// Update the cutted image of the frame, when a transformation is happend.
				//Take the drawn image from main canvas to offsetCanvas
					this.cutted.imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
					this.cutted.imageDataNoFill = ctx.getImageData(0,0,canvas.width,canvas.height);
				// Recovering the previous image into the main canvas
					ctx.putImageData(canvasImageData,0,0);
				// Updating cutted parameters
					this.cutted.backgroundColor = window.tools.secColor;
					this.buildNoFillImage();

					this.cutted.width = this.width;
					this.cutted.height = this.height;
					this.cutted.x = this.x;
					this.cutted.y = this.y;
			},
			flipImage: function(way){								// Flip transformation for the cutted image into the frame.
				var sW = 1;
				var sH = 1;
				this.translateX = 0;
				this.translateY = 0;
				// 	FLIP RECTIFICATIONS
					if ( way === 'horizontal' ){
						this.translateX = 2*this.x+this.width;
						sW = -1;
					}
					if ( way === 'vertical' ){
						this.translateY = 2*this.y+this.height;
						sH = -1;
					}

				// Clean the main canvas
					window.ctx.clearRect(0,0,canvas.width,canvas.height);
					window.offscreenCanvas.width = canvas.width;
					window.offscreenCanvas.height = canvas.height;
					window.offscreenCtx.putImageData(this.cutted.imageData,0,0);

				// 	DRAW 
					window.ctx.save();
						window.ctx.translate(this.translateX,this.translateY);
						window.ctx.scale(sW,sH);
						ctx.drawImage(offscreenCanvas,this.cutted.x,this.cutted.y,this.cutted.width,this.cutted.height,this.x,this.y,this.width,this.height);
					window.ctx.restore();
					this.updateCutted();
				// Draw 
					this.draw();
			},
			copyImage: function(){										// Put the cutted image into the image clipboard
				var cutted = this.cutted;
				var clipB = tools.localClipboardImg;

				clipB.imageData = ctx.createImageData(canvas.width,canvas.height);
		    	dataCopy =  clipB.imageData.data;
		    	data = this.cutted.imageData.data;
		    	
		    	for ( var i=0, end=data.length; i<end; i++ ){
		    		dataCopy[i] = data[i]; 

		    	}

		    	clipB.cutted.x = cutted.x;
				clipB.cutted.y = cutted.y;
				clipB.cutted.width = cutted.width;
				clipB.cutted.height = cutted.height;

			},
			pasteImage: function(){										// Get the image stored into the clipboard image and draw it into a new frame
																		// in the 0,0 canvas coordinates, deactivating the previous one if there was any.
				this.x = 0;
				this.y = 0;
				this.width = this.cutted.width = tools.localClipboardImg.cutted.width;
				this.height = this.cutted.height = tools.localClipboardImg.cutted.height;
				this.cutted.x = tools.localClipboardImg.cutted.x;
				this.cutted.y = tools.localClipboardImg.cutted.y;
				
				this.cutted.imageData = ctx.createImageData(canvas.width,canvas.height);
				this.cutted.imageDataNoFill = ctx.createImageData(canvas.width,canvas.height);

		    	dataCopy =  tools.localClipboardImg.imageData.data;
		    	data = this.cutted.imageData.data;
		    	dataNoFill = this.cutted.imageDataNoFill.data;

		    	for ( var i=0, end=dataCopy.length; i<end; i++ ){
		    		data[i] = dataCopy[i];
		    		dataNoFill[i] = dataCopy[i];
		    	}
		    	canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
				this.cutted.backgroundColor = tools.secColor;
				this.buildNoFillImage();

				this.cutted.is = true;
		    	this.active=true;
		    	draw();
			},
			invertSelectionColor: function(){							// Make and color inversion for the cutted image inside the frame.
				var cuttedData = this.cutted.imageData.data;
				for (var i=0, end=cuttedData.length; i<end; i+=4){
						if ( cuttedData[i+3] > 0 ){
							cuttedData[i] = 255 - cuttedData[i];
							cuttedData[i+1] = 255 - cuttedData[i+1];
							cuttedData[i+2] = 255 - cuttedData[i+2];
						}
				}
				this.buildNoFillImage();
				this.draw();
			},
			cutImage: function(){										// Get the image from the canvas to the cutted image for the frame.
					var cutted = this.cutted;
					if ( tools.selected === 0 ){
							this.cutZone = false;
						//Sizing the content box
							this.x = cutted.minX;
							this.y = cutted.minY;
							this.width = cutted.maxX - cutted.minX;
							this.height = cutted.maxY - cutted.minY;
						//Building the cutted shape for clip
							offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
							ctx.putImageData(canvasImageData,0,0);
							ctx.save();
							offscreenCtx.save();
								ctx.beginPath();
								offscreenCtx.beginPath();
							
									ctx.moveTo(cutted.manualCoords[0][0],cutted.manualCoords[0][1]);
									offscreenCtx.moveTo(cutted.manualCoords[0][0],cutted.manualCoords[0][1]);
							
									for ( var i=1,end=cutted.manualCoords.length; i<end; i++ ){
										ctx.lineTo(cutted.manualCoords[i][0],cutted.manualCoords[i][1]);
										offscreenCtx.lineTo(cutted.manualCoords[i][0],cutted.manualCoords[i][1]);
									}
								ctx.closePath();	
								offscreenCtx.closePath();
								
								ctx.clip();
								offscreenCtx.clip();

								offscreenCtx.drawImage(canvas,0,0,canvas.width,canvas.height);

								//Fill the cutted Shape (clipped) with secondary selected color
								ctx.fillStyle = tools.secColor;
								ctx.fillRect(0,0,canvas.width,canvas.height);
							
							ctx.restore();
							offscreenCtx.restore();	
						//Taking the image Data	
							cutted.imageData = offscreenCtx.getImageData(0,0,canvas.width,canvas.height);
							cutted.imageDataNoFill = offscreenCtx.getImageData(0,0,canvas.width,canvas.height);
					}else{
						this.bDraw = false;
						//Taking the image Data	
							cutted.imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
							cutted.imageDataNoFill = ctx.getImageData(0,0,canvas.width,canvas.height);
						//Fill the cutted Shape (rectangle) with secondary selected color
							ctx.putImageData(canvasImageData,0,0);
							ctx.fillStyle = window.tools.secColor;
							ctx.fillRect(this.x,this.y,this.width,this.height);	
					}
					canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
					cutted.backgroundColor = tools.secColor;
					this.buildNoFillImage();
					cutted.is = true;
					cutted.x = this.x;
					cutted.y = this.y;
					cutted.width = this.width;
					cutted.height = this.height;	
			},
			putCuttedImage: function(){															// Draw the cutted image into the canvas, inside the frame
				if ( this.cutted.width > 0 && this.cutted.height > 0 ){
					var cuttedImage;

					if ( !tools.backgroundTransp ){
						if ( this.cutted.backgroundColor !== tools.secColor ){
							this.cutted.backgroundColor = tools.secColor;
							this.buildNoFillImage();
						}
						cuttedImage = this.cutted.imageDataNoFill;
					}else{
						cuttedImage = this.cutted.imageData;
					}

					offscreenCanvas.width = cuttedImage.width;
					offscreenCanvas.height = cuttedImage.height;

					offscreenCtx.clearRect(0,0,offscreenCanvas.width,offscreenCanvas.height);
					offscreenCtx.putImageData(cuttedImage,0,0);
					
					try{
						ctx.drawImage(offscreenCanvas,this.cutted.x,this.cutted.y,this.cutted.width,this.cutted.height,this.x,this.y,this.width,this.height);
					}catch(error){
						console.info('ERROR:'+error,this.cutted.x,this.cutted.y,this.cutted.width,this.cutted.height,this.x,this.y,this.width,this.height);
					}
					offscreenCanvas.width = canvas.width;
					offscreenCanvas.height = canvas.height;
				}
			},
			buildNoFillImage: function(){													// Build an imageData version of the cutted image with a transparent background (selected secondary color)
				var dataNotFill = this.cutted.imageDataNoFill.data;
				var data = this.cutted.imageData.data;
				var color = colorToIntArray(this.cutted.backgroundColor);
				for ( var i=0, end=data.length; i<end; i+=4 ){
					dataNotFill[i] = data[i];
					dataNotFill[i+1] = data[i+1];
					dataNotFill[i+2] = data[i+2];
					if ( data[i]===color[0] && data[i+1]===color[1] && data[i+2]===color[2]){
						dataNotFill[i+3] = 0;
					}else if( data[i+3] > 250 ){
						dataNotFill[i+3] = 255;
					}
				}
			},
			drawResizePnt: function(pnt){													// Draw a resize point
				for ( i=0; i<this.resizePoints.length ; i++){
					if(this.resizePoints[i].p === pnt){
						ctx.rect(this.resizePoints[i].x,this.resizePoints[i].y,this.pointSize,this.pointSize);	
					}
				}
			},
			draw: function (){																// Main draw function, handle all neccesary stuffs to do so, calling other draw functions.
				var x = Math.round(this.x);
				var xd = x-1.5;
				var y = Math.round(this.y);
				var yd = y-1.5;
				var width = Math.round(this.width);
				var height = Math.round(this.height);
				var x2 = xd + width + 3;
				var y2 = yd + height + 3;

				// Blue FRAME width resize points
				if ( !this.bDraw && !this.cutZone ){
					ctx.beginPath();		
					// DashPoint rectangle
					ctx.drawDashedRect(xd,yd,x2,yd);
					ctx.drawDashedRect(x2,yd,x2,y2);
					ctx.drawDashedRect(x2,y2,xd,y2);
					ctx.drawDashedRect(xd,y2,xd,yd);
					
					// Coordinate points
					
					CY1 = y - this.pointSize;
					CY2 = y + Math.round((height)/2);
					CY3 = y + height;
				
					CX1 = x - this.pointSize;
					CX2 = x + Math.round((width)/2);
					CX3 = x + width;		
					
					this.resizePoints[0].x = CX2;//N
					this.resizePoints[0].y = CY1;
						this.drawResizePnt('n');
					this.resizePoints[1].x = CX3;//NE
					this.resizePoints[1].y = CY1;
						this.drawResizePnt('ne');
					this.resizePoints[2].x = CX3;//E
					this.resizePoints[2].y = CY2-1;
						this.drawResizePnt('e');
					this.resizePoints[3].x = CX3;//SE
					this.resizePoints[3].y = CY3;
						this.drawResizePnt('se');
					this.resizePoints[4].x = CX2-1;//S
					this.resizePoints[4].y = CY3;
						this.drawResizePnt('s');
					this.resizePoints[5].x = CX1;//SW
					this.resizePoints[5].y = CY3;
						this.drawResizePnt('sw');
					this.resizePoints[6].x = CX1;//W
					this.resizePoints[6].y = CY2-1;
						this.drawResizePnt('w');
					this.resizePoints[7].x = CX1;//NW
					this.resizePoints[7].y = CY1;
						this.drawResizePnt('nw');
				
					ctx.fillStyle='rgb(0,0,168)';
					ctx.fill();
				}

				// CUTTING ZONE
					if ( tools.selected === 0 && this.cutZone ){
						ctx.drawImage(offscreenCanvas,0,0,canvas.width,canvas.height);
					}
				// CUTTED IMAGES
					if ( tools.selected < 2 && this.cutted.is ){
						this.putCuttedImage();
					}
				// DUMMY LINES
					if ( this.sizeDummy.active || this.bDraw ){
						offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
						offscreenCtx.save();
							offscreenCtx.beginPath();
							offscreenCtx.lineWidth = ( this.cutted.is ) ? 1 : 4 ;
							if ( this.bDraw ){
								offscreenCtx.rect(this.x-2,this.y-2,this.width+4,this.height+4);
								offscreenCtx.strokeStyle = this.sizeDummy.fillPat[1];
							}else{
								offscreenCtx.rect(this.sizeDummy.x-2,this.sizeDummy.y-2,this.sizeDummy.width+4,this.sizeDummy.height+4);
								offscreenCtx.strokeStyle = this.sizeDummy.fillPat[0];
							}
							offscreenCtx.stroke();
						offscreenCtx.restore();

						window.drawResizeColor();

						ctx.drawImage(offscreenCanvas,0,0,canvas.width,canvas.height);
						offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
					}
			},
			reset: function(){																		// Reset the necessary variable and object to restart
				this.active = false;
				this.bDraw = false;
				this.cutZone = false;
				this.drag = false;
				this.rotateAng = 0;
				this.x = this.y = this.width = this.height = 0;
				this.cutted.is = false;
				this.cutted.x = this.cutted.y = this.cutted.width = this.cutted.height = 0;
				this.cutted.maxX = this.cutted.minX = this.cutted.maxY = this.cutted.minY = 0;
				this.cutted.manualCoords.length = 0;
			},
			deactivate: function(toolNum){															// Deactivate the frame, hidding it and leaving the cutted image over the canvas drawed
				var x = this.x,y = this.y;
				var width = this.width,height = this.height;
				var toolSel = toolNum || tools.selected;	

				if(this.width<0){			
					width = -this.width;
					x -= width;
				}
				if(this.height<0){			
					height = -this.height;
					y -= height;
				}

				if ( toolSel < 3 ){
					this.active = false;
					ctx.putImageData(canvasImageData,0,0);	
					this.putCuttedImage();
				}
				this.reset();	
				selecCursorChanger();
				window.tools.clearImage.changeState(true);
			},
			update:function(x,y){																	// Update manager for every size manipulating
				if ( this.bDraw ){
					this.setSize(x,y);
				}else if ( this.sizing ) {
					this.resizeBox(x,y);
				}else if( this.moving ){
					deltaX = x - tools.initX;
					deltaY = y - tools.initY
					this.x += deltaX;
					this.y += deltaY;
				}else if( this.cutZone ){
					this.manualCut(x,y);
				}
				if ( text.active ){
					text.updateCoords();
				}
			},
			updateSize: function(){																	// Update a resize action
				var dummy = this.sizeDummy;
				var minHeight = 0;
				var minWidth = 0;
				// FOR TEXT TOOL
					if ( tools.selected === 9){
						minHeight = Math.round(window.textTool.size.selected*1.67+2);
						minWidth = Math.round(ctx.measureText('abcd').width);
					}
				//WIDTH CONTROL	
				if ( dummy.width > minWidth ){
					this.x = Math.round(dummy.x);
					this.width = Math.round(dummy.width);
				}else{
					if ( this.x != dummy.x ){
						this.x = Math.round(dummy.x - minWidth + dummy.width);
					}
					this.width = minWidth;
				}
				// HEIGHT CONTROL
				if ( dummy.height > minHeight ){
					this.y = Math.round(dummy.y);
					this.height = Math.round(dummy.height);
				}else{
					if ( this.y != dummy.y ){
						this.y = Math.round(dummy.y - minHeight + dummy.height);
					}
					this.height = minHeight;
				}
				dummy.reset();
				// FOR TEXT TOOL
					if ( text.active && tools.selected === 9 ){
						text.updateCoords();
					}
			},
			sizeDummy:{	x:0,y:0,width:0,height:0,active:false,fillPat:[],						// Handle the dummy for the resize action
						reset: function(){
							this.x = this.y = this.width = this.height = 0;
							this.active = false;
						}
			},
			setSize: function(x,y){																// When user make a mouseup after a size action, this one update the frame size
				var deltaX = x - tools.initX;
				var deltaY = y - tools.initY;

				if ( x >= 0 && x <= canvas.width && y >= 0 && y <= canvas.height ){
				   if ( deltaX < 0 ){
				   		deltaX = - deltaX;
						this.x = x;	
					}
					if ( deltaY < 0 ){
				   		deltaY = - deltaY;
						this.y = y;
					}	
					this.width = deltaX;
					this.height = deltaY;	
				}
				updateDimBox(deltaX,deltaY);
			},
			resizeBox: function(x,y){															// This method handle the dummy resizing when the user us dragging a resize pointer
				var deltaX = tools.initX - x;
				var deltaY = tools.initY - y;
				var sizeDummy = this.sizeDummy;

				if ( x >= 0 && x <= canvas.width && y >= 0 && y <= canvas.height ){
					if ( !sizeDummy.active ){
						sizeDummy.active = true;
						sizeDummy.x = this.x;
						sizeDummy.y = this.y;
						sizeDummy.width = this.width;
						sizeDummy.height = this.height;
					}
					if ( (this.pointOver === 'sw' || this.pointOver === 'w' || this.pointOver === 'nw') ){
						sizeDummy.width += deltaX;
						sizeDummy.x = x;
					}
					if ( this.pointOver === 'ne' || this.pointOver === 'e' || this.pointOver === 'se' ){
						sizeDummy.width -= deltaX;
					}
					if ( this.pointOver === 'nw' || this.pointOver === 'n' || this.pointOver === 'ne' ){
						sizeDummy.y = y;
						sizeDummy.height += deltaY;
					}
					if ( this.pointOver === 'se' || this.pointOver === 's' || this.pointOver === 'sw' ){
						sizeDummy.height -= deltaY;
					}

					updateDimBox(sizeDummy.width,sizeDummy.height);
				}	
			}					
		}
//////////////////////////////////// END-[TOOLS VARIABLES] //////////////////////////////////////////////////////


//////////////////////////////////// TITLE //////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
//	CLASS - CustomTitle 1.0.0														//
//		Add mouseover & mouseout to every element with sClassName.					//
//		When mouseOver event is fired fnIn callback function is called 'nTime' 		//
//		millisecons later.The same for fnOut function when mouseout event is fired.	//
//		-	callbackIn: Function called nTime ms later from mouseover event.		//
//		-	callbackOut: Function called with mouseout event (setTimeout is cleared)//
//		-	nDelay: (optional) time in milliseconds from mouseover firing to  		//
//					callbackIn calling.	[500ms by default]							//
//		-	sName: (optional) string with the css class name for DOM elements  		//
//					localization. ['custom-title' by default]						//
//////////////////////////////////////////////////////////////////////////////////////  
var CustomTitle = function(callbackIn,callbackOut,nDelay,sName){
	var self = this;
	var nTime = nDelay || 500;
	var sClassName = sName || 'custom-title';
	var fnIn = callbackIn;
	var fnOut = callbackOut;
	var pTimer = null;

	if ( isNaN(nTime) ) nTime = 500;
	// Adding event listeners
	var elements = document.getElementsByClassName(sClassName);	
	for ( var i=0,end=elements.length; i<end; i++ ){
		addEvent(elements[i],'mouseover',function(e){self.in(e)},false);
		addEvent(elements[i],'mouseout',function(e){self.out(e)},false);
	}
	//Mouseover function
	this.in = function(e){
		var eventObjIn = trigger(e);
		if ( pTimer != null ){
			clearInterval(pTimer);
			pTimer = null;
		}
		pTimer = setTimeout(function(){
										clearInterval(pTimer);
										pTimer = null;
										fnIn(eventObjIn);
								},nTime);
	};
	//Mouseout function
	this.out = function(e){
		var eventObjOut = trigger(e);
		//Clean timer
			if (pTimer != null ){
				clearInterval(pTimer);
				pTimer = null;
			}
		//Call out function
			if ( fnOut ){
				fnOut(eventObjOut);
			}	
	}
};

		//////////////////////////////////////////////////////////////////////////////////////
		//	OBJECT - showElTitle															//
		//		Show the title element with the name of the tool referenced by the target 	//
		//		element ( element ) 														//
		//////////////////////////////////////////////////////////////////////////////////////  
		var showElTitle = function(element){
			var elBound = element.getBoundingClientRect();
			var dataSet = element.dataset;
			var toolId = ( dataSet ) ? parseInt(dataSet.toolid) : element.getAttribute('data-toolid');
			var label = document.getElementById('title_label');

			document.getElementById('comments_box').value = titlesInfo[toolId].phrase;
			
			if ( !label ){
				label = document.createElement('DIV');		
				label.id = "title_label";
				label.classList.add('title_label');
				document.body.appendChild(label);
			}
			label.style.display = 'block';
			label.style.top = (elBound.top+elBound.height+3).toString()+'px';
			label.style.left = elBound.left.toString()+'px';
			label.innerHTML = titlesInfo[toolId].title;
		}

		//////////////////////////////////////////////////////////////////////////////////////
		//	OBJECT - hideElTitle															//
		//		Hide the title element show before by showElTitle objest, with the name of 	//
		//		the tool referenced.														//
		//////////////////////////////////////////////////////////////////////////////////////  
		var hideElTitle = function(element){
			var phrase = window.tools.selected ? window.titlesInfo[window.tools.selected].phrase : '';
			var label = document.getElementById('title_label');

			document.getElementById('comments_box').value = phrase;
			if ( label ){
				label.style.display = 'none';
			}	
		}
//////////////////////////////////// END-TITLE //////////////////////////////////////////////////////


//////////////////////////////////// 	MODAL WINDOW OBJECTS 	////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - windowsDragging														//
//		Manage the move of the elements with 'window' CSS class name since mouse 	//
//		click and move over the modal window header.								//
////////////////////////////////////////////////////////////////////////////////////// 
var windowsDragging = {
	bClicked:false,		// Boolean to know is header clicked or not
	elClicked:null,		// DOM element pointer to the modal window which is clicked
	initX:0,			// Init X/left coord when the mouse click down is done
	initY:0,			// Init Y/top coord when the mouse click down is done
	top:0,				// Distance of the current modal window to top in pixels
	left:0,				// Distance of the current modal window to left in pixels

	// Set the data to move the modal window in which header has been clicked
	mousedown: function(e){
		var self = window.windowsDragging;
		var coords = mouseCoord(e);
		var container = trigger(e);
		poscontainer = null;
		self.bClicked = true;
		self.initX = coords.x;
		self.initY = coords.y;
		do{
			container = container.parentNode;
		}while( container.className.indexOf('window') === (-1) );
		
		self.elClicked = container;
		poscontainer = container.getBoundingClientRect();
		
		self.top = poscontainer.top;
		self.left = poscontainer.left;
	},
	// Reset the data to end the move actions
	mouseup:function(){
		var self = window.windowsDragging;
		self.bClicked = false;
		self.elClicked = null;
		self.top = self.left = self.initX = self.initY = 0;
	},
	// Move the modal window as mouse has been moved
	mousemove: function(e){
		var self = window.windowsDragging;
		if ( self.elClicked ){
			var coords = mouseCoord(e);
			deltaX = coords.x - self.initX;
			deltaY = coords.y - self.initY;
			self.elClicked.style.top = (self.top+deltaY).toString() +'px';
			self.elClicked.style.left = (self.left+deltaX).toString() +'px';
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - edit 																	//
//		Have the methods to the enabling toggle of the edit and context menu 		//
//		actions. This controls what is allowed to the user in every moment.			//
////////////////////////////////////////////////////////////////////////////////////// 
var edit = {
	enable: function(actionName){

		var submenuAnc = document.getElementById('edit_'+actionName);
		var ctxmenuAnc = document.getElementById('ctx_'+actionName);
		if ( actionName !== 'copyto')
			submenuAnc.className = submenuAnc.classList.remove('disabled');
		if ( actionName !== 'undo' && actionName !== 'repeat')
			ctxmenuAnc.className = ctxmenuAnc.classList.remove('disabled');

	},
	disable: function(actionName){
		var submenuAnc = document.getElementById('edit_'+actionName);
		var ctxmenuAnc = document.getElementById('ctx_'+actionName);
		if ( actionName !== 'copyto')
			submenuAnc.classList.add('disabled');
		if ( actionName !== 'undo' && actionName !== 'repeat')
			ctxmenuAnc.classList.add('disabled');
	}
};

//////////////////////////////////////////////////////////////////////////////////////
//	FUNCTION - clearSelect															//
//		Clear select menu action. This fill all canvas fill the secondary color		//
//		selected or call "deleteKey" method of the text object when a text is		//
//		editing.																	//
////////////////////////////////////////////////////////////////////////////////////// 
function clearSelect(){
	if ( tools.selected === 9 && text.cursor.active){
		window.text.deleteKey();
	}
	// Cut the selected Image
	if ( tools.selected < 2 && selectionFrame.active ){
	   	selectionFrame.cutted.imageData = ctx.createImageData(canvas.width,canvas.height);
	   	selectionFrame.cutted.imageDataNoFill = ctx.createImageData(canvas.width,canvas.height);
	   	selectionFrame.deactivate();
    }
}

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - flipRotate																//
//		Manage the flip horizontal||vertical and rotate [90º,180º,270º] of the 		//
//		entire canvas. There is another one equivalent to this in the 				//
//		selectionFrame Object for image portion manipulation.						//
////////////////////////////////////////////////////////////////////////////////////// 
var flipRotate = {
				shown:false,								// Tell us if the flipRotate modal window is shown or not.
				selected:{type:'flip',mode:'horizontal'},	// Store the action selected into the modal window to apply.
				winEl:null,									// Pointer to the modal window DOM element.
				okBtn:null,									// Pointer to the OK btn DOM element.
				rotateAng:'90',								// Store the rotation selected.
				cancelBtn:null,								// Pointer to the Cancel btn DOM element.
				closeBtn:null,								// Pointer to the Close btn DOM element.

				showToggle: function(){
					if ( window.flipRotate.shown ){
						window.flipRotate.winEl.style.display = "none";
						window.flipRotate.shown = false;
					}else{
						window.flipRotate.winEl.style.display = "block";
						window.flipRotate.shown = true;
					}
				},
				// Manage the modal window actions over the options by the user.
				optSelection: function(e){
					var el = trigger(e);
					var disableRotate = false;
					var change = false;
					var inputs = document.getElementsByName('rotate');
					var self = window.flipRotate;
					
					self.selected.mode = el.value;

					if ( el.nodeName === 'INPUT'){
						if ( (el.value === 'horizontal' || el.value === 'vertical') && self.selected.type === 'rotate' ){
							self.selected.type = 'flip';
							disabbleRotate = true;
							change = true;
						}else if ( el.value === 'rotate' && self.selected.type === 'flip' ){
							self.selected.type = 'rotate';
							disabbleRotate = false;
							change = true;
						}else if ( el.name === 'rotate' ){
							self.rotateAng = el.value;
						}
						if ( change ){
							for ( var i=0,end=inputs.length; i<end; i++){
								var input = inputs[i];
								var label = input.nextSibling;
								while (label.nodeType != 1){
							  		label=label.nextSibling;
							  	}
								if ( disabbleRotate ){
									label.classList.add('disabled');
									input.disabled = true;
								}else{
								 	label.classList.remove('disabled');
									input.disabled = false;
								}
							}
						}
					}	
				},
				flip:function(mode){
					if ( window.selectionFrame.active ){
						window.selectionFrame.flipImage(mode);
					}else{
						var fh = mode === 'horizontal' ? -1 : 1;
						var fv = mode === 'vertical' ? -1 : 1;
						var x = fh < 0 ? window.canvas.width*fh : 0;
						var y = fv < 0 ? window.canvas.height*fv : 0;

						window.offscreenCtx.clearRect(0,0,window.canvas.width,window.canvas.height);
						window.offscreenCtx.putImageData(window.canvasImageData,0,0);
						window.ctx.save();
							window.ctx.scale(fh,fv);
							window.ctx.drawImage(window.offscreenCanvas,x,y,window.canvas.width,window.canvas.height);
						window.ctx.restore();	
						window.saveImage();
					}
				},
				rotate:function(mode){
					window.offscreenCtx.clearRect(0,0,window.canvas.width,window.canvas.height);
					window.offscreenCtx.putImageData(window.canvasImageData,0,0);
					var width =  window.canvas.width;
					var height =  window.canvas.height;
					var transW = width;
					var transH = height;

					
					if ( !isNaN(mode) ){
						if ( mode !== Math.PI){
							transH = mode < Math.PI ? 0 : width;
							transW = mode < Math.PI ? height : 0;
							canvas.width = height;
							canvas.height = width;
							window.canvasAttributes.update();
							window.canvasChangeBoxSize(height,width);	
						}	
							ctx.save();
								ctx.translate(transW,transH);
								ctx.rotate(mode);
								window.ctx.drawImage(window.offscreenCanvas,0,0,width,height);
							ctx.restore();
						
						
						offscreenCanvas.width = canvas.width;
						offscreenCanvas.height = canvas.height;

						window.saveImage();
					}	
				},
				// This method is fired when OK btn is clicked and decide what method call (flip||rotate)
				action: function(e){
					if ( window.flipRotate.selected.mode === 'horizontal'){
						window.flipRotate.flip('horizontal');
					}else if ( window.flipRotate.selected.mode === 'vertical'){
						window.flipRotate.flip('vertical');
					}else{
						var angRad = window.flipRotate.rotateAng*Math.PI/180;
						if ( window.selectionFrame.active ){
							window.selectionFrame.rotateImage(angRad);
						}else{
							window.flipRotate.rotate(angRad);
						}						
					}
					window.flipRotate.showToggle();
				}
};

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - stretchSkew															//
//		Manage the skew(º) and strech(%) of the entire canvas.				 		//
//		There is another one equivalent to this in the selectionFrame Object for 	//
//		image portion manipulation.													//
////////////////////////////////////////////////////////////////////////////////////// 
var stretchSkew = {
		elementWin:null,
		shown:false,
		stretchH:null,
		stretchV:null,
		skewH:null,
		skewV:null,
		action: function(){
			var self = window.stretchSkew;
			var stH = self.stretchH.value;
			var stV = self.stretchV.value;
			var skH = self.skewH.value;
			var skV = self.skewV.value;
			var error=0;

			//VALIDATIONS
				if ( isNaN(skH) || skH < -89 || skH > 89 || skH == ''){
					skH = 0;
					self.skewH.value = 0;
					error = 2;
				}
				if ( isNaN(skV) || skV < -89 || skV > 89 || skV == ''){
					skV = 0;
					self.skewV.value = 0;
					error = 2;
				}
				if ( isNaN(stH) || stH < 1 || stH > 500 || stH == '' ){
					stH = 100;
					self.stretchH.value = 100;
					error = 1;
				}	
				if ( isNaN(stV) || stV < 1 || stV > 500 || stV == '' ){
					stV = 100;
					self.stretchV.value = 100;
					error = 1;
				}
			// CALLS
				if ( error === 0 ){
					if ( window.selectionFrame.active ){
						window.selectionFrame.stretchSkew(skH,skV,stH,stV);
					}else{
						var zoom = window.tools.magnifier.zoomMag;
						var hrad = Math.PI*skH/180;
						var vrad = Math.PI*skV/180;
						var skewh = hrad === 0 ? 0 : Math.tan(hrad);
						var skewv = vrad === 0 ? 0 : Math.tan(vrad);
						// DIMENSION RESULT OF CANVAS
							var newWidth = Math.round((canvas.width*stH/100 + Math.abs(skewh*canvas.height)));
							var newHeight = Math.round((canvas.height*stV/100 + Math.abs(skewv*canvas.width)));
							var newX = 0;
							var newY = 0;
							if ( skewh < 0 ){
								newX -= Math.round(skewh*canvas.height);
								newY -= Math.round(Math.atan(skewv)*newX);
							}
							if ( skewv < 0 ){							
								newY -= Math.round(skewv*canvas.width);
								newX -= Math.round(Math.atan(skewh)*(newY));
							}
						// DIMESION UPDATING
						window.canvas.width = newWidth;
						window.canvas.height = newHeight;

						// DRAW THE NEW IMAGE
							offscreenCtx.putImageData(canvasImageData,0,0);
							window.ctx.fillStyle = window.tools.secColor;
							window.ctx.fillRect(0,0,newWidth,newHeight);

							window.ctx.save();
								window.ctx.transform(stH/100,skewv,skewh,stV/100,0,0);
								window.ctx.drawImage(offscreenCanvas,newX,newY,window.offscreenCanvas.width,window.offscreenCanvas.height);
							window.ctx.restore();
						//UPDATING OFFSET & CANVAS BOX
							window.offscreenCanvas.width = newWidth;
							window.offscreenCanvas.height = newHeight;
						
							window.canvasChangeBoxSize(Math.round(newWidth*zoom),Math.round(newHeight*zoom));
							window.canvasAttributes.update();

						window.saveImage();
					}
					self.skewH.value = 0;
					self.skewV.value = 0;
					self.stretchH.value = 100;
					self.stretchV.value = 100;
					self.showToggle();
				}else{
					var msg = 'Please enter an integer between 1 and 500';
					if ( error === 2 )
						msg = 'Please enter an integer between -89 and 89';

					window.windowMsg.show(1,msg,['OK'],[0],false);
				}
		},
		showToggle:function(){
			if ( window.stretchSkew.shown ){
				window.stretchSkew.elementWin.style.display = "none";
				window.stretchSkew.shown = false;
			}else{
				window.stretchSkew.elementWin.style.display = "block";
				window.stretchSkew.shown = true;
			}
		}
};

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - canvasAttributes														//
//		Manage canvas size attributes and image file name.					 		//
//		So it is used when a canvas resize is needed, by the attributes modal 		//
//		window inputs, canvas resize points dragging...								//
////////////////////////////////////////////////////////////////////////////////////// 
var canvasAttributes = {
	element:null,
	widthEl:null,
	height:null,
					// Hide/Show attributes modal window
	hide: function(){	
		window.canvasAttributes.element.style.display = 'none';
	},
	show: function(){
		window.canvasAttributes.element.style.display = 'block';
	},
	changeFileName: function(fileName){								
		var nameParts = fileName.split('.');
		var numParts = nameParts.length;
		var newName = '';
		
		// File extension is taken away
		if ( numParts > 1 ) {
			for( var i=0,end=numParts-1; i<end; i++ ){
				newName += nameParts[i];
			}
		}else{
			newName = fileName;
		}
		
		// Dangerous simbols cleaning
		newName = newName.replace('<','');
		newName = newName.replace('>','');
		newName = newName.replace('/','');
		newName = newName.replace('=','');
		document.getElementById('attr_name').value = newName;
		document.getElementById('header_title').innerHTML = newName;
		window.filename = newName;
	},
	change: function(w,h){
		var newWidth = w || window.canvasAttributes.widthEl.value;
		var newHeight = h || window.canvasAttributes.heightEl.value;
		var writtenName = document.getElementById('attr_name').value;
		var zoom = window.tools.magnifier.zoomMag;
		
		if ( window.canvas.width != newWidth || window.canvas.height != newHeight ){
			// CHANGE THE CANVAS PIXEL SIZE
				window.canvas.width = newWidth;
				window.canvas.height = newHeight;
				window.canvasAttributes.update();
		
			// UPDATING CURRENT IMAGE
				window.ctx.fillStyle = '#FFFFFF';
				window.ctx.fillRect(0,0,newWidth,newHeight);
				window.offscreenCtx.putImageData(canvasImageData,0,0);
				window.ctx.drawImage(window.offscreenCanvas,0,0,window.offscreenCanvas.width,window.offscreenCanvas.height);
		
			// UPDATING IMAGE DATA & SAVE IMAGE
				window.canvasImageData = window.ctx.getImageData(0,0,window.canvas.width,window.canvas.height);
				window.savedImagesData.images[window.savedImagesData.ind] = window.canvasImageData;
				window.offscreenCanvas.width = window.canvas.width;
				window.offscreenCanvas.height = window.canvas.height;

				window.canvasChangeBoxSize(window.canvas.width*zoom,window.canvas.height*zoom);
		}

		if ( writtenName != window.filename ){
			window.canvasAttributes.changeFileName(writtenName);
		}

		window.canvasAttributes.hide();
	},
	update: function(){
		window.canvasAttributes.widthEl.value = window.canvas.width;
		window.canvasAttributes.heightEl.value = window.canvas.height;
	}
};

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - editColors																//
//		This is the handler of the hardest modal window, where you can make your	//
//		colors using RGB or HSL values, or picking directly from a color card into  //
//		a specific canvas for that.													//
//////////////////////////////////////////////////////////////////////////////////////
var editColors = {
	element:null,			// Window DOM element
	mousepress:false,		// Mouse pressed over color card
	lumiPress:false,
	active:false,			// WINDOW SHOWN
	define:false,			// RIGHT SIDE OPEN
	colorTable:new Image(),	// COLOR CARD
	colorCanvas:null,		// CANVAS
	colorCtx:null,			// CTX CANVAS
	lumiPointer:null,		// Pointer of the canvas luminance indicator DOM element
	rihtBtnTime:0,			// Time of the last right click

	clrBoxSel:{type:'basic',basic:null,custom:null},
	clrBrushSel:{rightBtn:false,element:null},

	hslSel:[0,1,0.5],		// Current Hue,Sat,lumi
	rgbSel:[255,0,0],		// Current R,G,B
	coordSel:[0,0,93],		// Current Coord over the color card
	colorSelEl:null,
	HSLSelEL:{h:null,s:null,l:null},
	RGBSelEl:{r:null,g:null,b:null},

	showToggle: function(){	
		var self = window.editColors;
		if ( window.editColors.active ){
			window.editColors.element.style.display = 'none';
			window.editColors.active = false;
			self.element.classList.remove('define');
			document.getElementById("color_define").classList.remove('disable');
		}else{
			window.editColors.element.style.display = 'block';
			window.editColors.active = true;
			window.editColors.drawBack();
		}
	},
	showDefine:function(){
		var self = window.editColors;
		if ( !self.define ){
			document.getElementById("color_define").classList.add('disable');
			self.element.classList.add('define');
			self.drawLumiBar();
			self.drawPointer(self.coordSel[0],self.coordSel[1]);
		}
	},
	action:function(){
		var self = window.editColors;
		var col = self.rgbSel;
		var strColor = 'rgb('+col[0]+','+col[1]+','+col[2]+')';
		self.clrBrushSel.element.style.backgroundColor = strColor;
		window.changeColor(strColor,!self.clrBrushSel.rightBtn);

		self.showToggle();
	},
	addColor: function(){
		var self = window.editColors;
		var col = self.rgbSel;
		self.clrBoxSel['custom'].style.backgroundColor = 'rgb('+col[0]+','+col[1]+','+col[2]+')';
	},
	selectColorBox: function(e,from){
		var element = trigger(e);
		
		if ( element.className.indexOf('basic_color') > 0 ){
			var self = window.editColors;

			//Selection Class	
				self.clrBoxSel[self.clrBoxSel.type].classList.remove('selected');
				self.clrBoxSel.type = from;
				self.clrBoxSel[from] = element;

				element.classList.add('selected');

			//Take the RGB color
				var colorStyle = element.style.backgroundColor.replace('rgb(','');
					colorStyle = colorStyle.replace(')','');
				var	RGB = colorStyle.split(',');
			//Update edit colors
				self.updateRGB(RGB[0],RGB[1],RGB[2]);
				var HSL = self.rgbtohsl(RGB[0],RGB[1],RGB[2]);
				self.updateHSL(HSL[0],HSL[1],HSL[2]);
				self.updateCoords();
				self.updateColorSel();
				self.drawLumiBar();
		}
	},
	updateCoords: function(){
		var self = window.editColors;
		self.coordSel[0] = Math.round(self.hslSel[0]*174);
		self.coordSel[1] = Math.round((1-self.hslSel[1])*186); 
		self.coordSel[2] = Math.round((1-self.hslSel[2])*186); 
		self.lumiPointer.style.top = self.coordSel[2].toString() + 'px'; 
		self.drawPointer();
		self.drawLumiBar();
	},
	updateRGB: function(r,g,b){
		var self = window.editColors;
		self.rgbSel = [r,g,b];
		self.RGBSelEl.r.value = r;
		self.RGBSelEl.g.value = g;
		self.RGBSelEl.b.value = b;
	},
	updateHSL: function(h,s,l){
		var self = window.editColors;
		self.hslSel = [h,s,l];
		self.HSLSelEL.h.value = Math.round(h*240);
		self.HSLSelEL.s.value = Math.round(s*240);
		self.HSLSelEL.l.value = Math.round(l*240);
	},
	updateColorSel: function(){
		var self = window.editColors;
		self.colorSelEl.style.backgroundColor = 'rgb('+self.rgbSel[0]+','+self.rgbSel[1]+','+self.rgbSel[2]+')';
	},
	// COORDS TO VALUES
	takeColor: function(e){
		var self = window.editColors;
		if ( self.mousepress ){
			var coords = coordIntoCanvas(self.colorCanvas,e);
			var ctx = self.colorCtx;
			var data = ctx.getImageData(coords.x,coords.y,1,1).data;
			var h = self.hslSel[0];
			var s = self.hslSel[1];
			var l = self.hslSel[2];

			if ( coords.x < 175 ){
				self.coordSel[0] = coords.x;
				self.coordSel[1] = coords.y;
				self.drawPointer();

				h = coords.x/174;
				s = 1-coords.y/186;
			}else if (coords.x > 190 ){
				self.coordSel[2] = coords.y;
				self.lumiPointer.style.top = coords.y.toString() + 'px';
				l = 1-coords.y/186;
			}	

			var rgb = self.hsltorgb(h,s,l);
			// UPDATING
				self.updateHSL(h,s,l);
				self.updateRGB(rgb[0],rgb[1],rgb[2]);
				self.updateColorSel();
				self.drawLumiBar();
		}
	},
	takeLumi: function(e){
		var self = window.editColors;
		if ( self.lumiPress ){
			var element = document.getElementById('lumibar_container');
			var coords = coordIntoElement(element,e);
			if (  coords.y > 4 && coords.y < 192 ){
				var coordY = coords.y-5;
				self.coordSel[2] = coordY;
				self.lumiPointer.style.top = coordY.toString() + 'px';
				var h = self.hslSel[0];
				var s = self.hslSel[1];
				var l = 1-coordY/186;

				var rgb = self.hsltorgb(h,s,l);
				// UPDATING
					self.updateHSL(h,s,l);
					self.updateRGB(rgb[0],rgb[1],rgb[2]);
					self.updateColorSel();
					self.drawLumiBar();
			}
		}
	},
	takeRGB:function(e){
		var self = window.editColors;
		var el = trigger(e);
		var id = el.id;
		// Value Validation
			if ( el.value > 255 )
				el.value = 255;
			if ( isNaN(el.value) || el.value < 0 )
				el.value = 0;
		// Take value
			if ( id === 'defined_red' )
				self.rgbSel[0] = el.value;
			if ( id === 'defined_green' )
				self.rgbSel[1] = el.value;
			if ( id === 'defined_blue' )
				self.rgbSel[2] = el.value;
		// Update rest of elements ( HSL,Coords,color )
			var HSL = self.rgbtohsl(self.rgbSel[0],self.rgbSel[1],self.rgbSel[2]);
			self.updateHSL(HSL[0],HSL[1],HSL[2]);
			self.updateCoords();
			self.updateColorSel();
	},
	takeHSL:function(e){
		var self = window.editColors;
		var el = trigger(e);
		var id = el.id;
		// Value Validation
			if ( el.value > 240 )
				el.value = 240;
			if ( isNaN(el.value) || el.value < 0 )
				el.value = 0;
		// Take value
			if ( id === 'defined_hue' )
				self.hslSel[0] = parseInt(el.value)/240;
			if ( id === 'defined_sat' )
				self.hslSel[1] = parseInt(el.value)/240;
			if ( id === 'defined_lum' )
				self.hslSel[2] = parseInt(el.value)/240;
		// Update rest of elements ( HSL,Coords,color )
			var RGB = self.hsltorgb(self.hslSel[0],self.hslSel[1],self.hslSel[2]);
			self.updateRGB(RGB[0],RGB[1],RGB[2]);
			self.updateCoords();
			self.updateColorSel();

	},
	hsltorgb:function(h,s,l){
		//http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
			var r, g, b;

		    if(s == 0){
		        r = g = b = l; // achromatic
		    }else{
		        function hue2rgb(p, q, t){
		            if(t < 0) t += 1;
		            if(t > 1) t -= 1;
		            if(t < 1/6) return p + (q - p) * 6 * t;
		            if(t < 1/2) return q;
		            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
		            return p;
		        }

		        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		        var p = 2 * l - q;
		        r = hue2rgb(p, q, h + 1/3);
		        g = hue2rgb(p, q, h);
		        b = hue2rgb(p, q, h - 1/3);
		    }

		    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
	},
	rgbtohsl:function(r,g,b){
		//http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
		 	r /= 255, g /= 255, b /= 255;
		    var max = Math.max(r, g, b), min = Math.min(r, g, b);
		    var h, s, l = (max + min) / 2;

		    if(max == min){
		        h = s = 0; // achromatic
		    }else{
		        var d = max - min;
		        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
		        switch(max){
		            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
		            case g: h = (b - r) / d + 2; break;
		            case b: h = (r - g) / d + 4; break;
		        }
		        h /= 6;
		    }

		    return [h, s, l];
	},
	drawPointer: function(){
		var self = window.editColors;
		var ctx = self.colorCtx;
		var x = self.coordSel[0];
		var y = self.coordSel[1];
		window.editColors.drawBack();
		ctx.fillStyle = '#000000';
		ctx.fillRect(x-9,y-1,5,3);
		ctx.fillRect(x+5,y-1,5,3);
		ctx.fillRect(x-1,y-9,3,5);
		ctx.fillRect(x-1,y+5,3,5);
	},
	drawBack: function(){
		if (window.editColors.colorTable.complete){
			window.editColors.colorCtx.drawImage(window.editColors.colorTable,0,0,175,187);
		}
	},
	drawLumiBar: function(){
		self = window.editColors;
		for ( var i=0; i<187; i++ ){
			var rgb =self.hsltorgb(self.hslSel[0],self.hslSel[1],i/186);
			self.colorCtx.fillStyle = 'rgb('+rgb[0]+','+rgb[1]+','+rgb[2]+')';
			self.colorCtx.fillRect(192,186-i,10,1);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - textTool																//
//		This object handle the text toolbar. So control and store the values and	//
//		actions of the user over this modal window ( font-size, family-size, 		//
//		font style).																//
//////////////////////////////////////////////////////////////////////////////////////			
var textTool = {
				menuElement:null,
				toolbar:{ 
						element:null,
						header:null,
						shown:true,
						selected:true
				},	
				font:{	
						listElement:null,
						inputText:null,
						listShown:false, 
						selected:"Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif"
				},
				size:{	
						listElement:null,
						inputText:null,
						listShown:false,
						selected:8
				},
				style:{
						bold:false,
						cursive:false,
						underline:false
				},
				changeShown: function(){
					var cell = window.get_firstchild(this.menuElement);
					if ( this.toolbar.shown ){	// HIDE
						cell.className = '';
						this.toolbar.shown = false;
						this.toolbar.element.style.display = 'none';

					}else{						//SHOW
						cell.className = 'check';
						this.toolbar.element.style.display = 'block';
						this.toolbar.shown = true;
					}
				},
				activate: function(){
					this.menuElement.className = '';
					if ( this.toolbar.shown ){
						this.toolbar.element.style.display = 'block';
					}
				},
				deactivate: function(){
					this.menuElement.className = 'disabled';
					if ( this.toolbar.shown ){
						this.toolbar.element.style.display = 'none';
					}
				},
				select: function(){
					if ( this.toolbar.selected ){
						this.toolbar.selected = false;
						this.toolbar.header.className = this.toolbar.header.className.replace(' tSelected','');
						window.text.cursor.active = true;
					}else{
						this.toolbar.selected = true;
						this.toolbar.header.className += ' tSelected';
						window.text.cursor.active = false;
					}	
				},
				handleTextSize: function(e){
					var self = window.textTool;
					var value = self.size.inputText.value;

					if ( value.length === 0 ){
						self.size.inputText.value = self.size.selected;
					}else if ( isNaN(value) ){
						window.windowMsg.show(1,'The font size must be a numeric value',['OK'],[0],false);
						self.size.inputText.value = self.size.selected;
					}else if ( self.size.selected !== parseInt(value) ){
						self.size.selected = value;
						self.updateFont();
					}
					return window.preventDefault(e);
				},
				handleSizeList: function(e){
					var self = window.textTool;
					var element = trigger(e).innerHTML;
					if ( !isNaN(element) ){
						window.textTool.size.inputText.value = element;
						window.textTool.size.selected = element;
						self.updateFont();
					}
					return window.preventDefault(e);
				},
				handleFontList: function(e){
					var self = window.textTool;
					var element = trigger(e);
					var value = element.getAttribute('data-fontFmly');

					if ( value ){
						self.font.selected = value;
						self.font.inputText.innerHTML = element.innerHTML;
						
						var fontListElement = self.font.listElement.getElementsByClassName('select_item');
						for ( var i=0, end = fontListElement.length; i<end; i++){
							fontListElement[i].className = fontListElement[i].className.replace(' iselected','');
						}
						element.className += ' iselected';

						self.updateFont();
					}
					return window.preventDefault(e);
				},
				mousedownToolbar: function(e){
					var element = trigger(e);
					var from = element.id;
					var self = window.textTool;
					var toolbar = self.toolbar;
					
					//SELECT TOOLBAR IF NOT
					if ( !toolbar.selected ){
						self.select();
					}
					// CLOSE TOOLBAR
					if ( from === 'text_close'){
						self.changeShown();
					}
					// SHOW FONTS LIST
					if ( from === 'font_btn'){
						var font = self.font;

						if (font.listShown ){
							font.listShown = false;
							font.listElement.style.display='none';
						}else{
							font.listShown = true;
							font.listElement.style.display='block';
						}
					}
					//SHOW SIZE LIST
					if ( from === 'size_btn'){
						var size = self.size;

						if ( size.listShown ){
							size.listShown = false;
							size.listElement.style.display='none';
						}else{
							size.listShown = true;
							size.listElement.style.display='block';
						}
					}
					// CHANGE FONT STYLE
					if ( from.match(/select_img/) ){
						var styleType = self.style;
						var parts = from.split('_');
						var button = document.getElementById(parts[0]+'_'+parts[1]);

						if ( styleType[parts[0]] ){
							button.className = button.className.replace (' inset selected',' outset');
						}else{
							button.className = button.className.replace (' outset',' inset selected');
						}
						styleType[parts[0]] = !styleType[parts[0]];
						self.updateFont();

					}
				},
				hideList: function(){
					self = window.textTool;
					var size = self.size;
					var font = self.font;
					if ( size.listShown ){
							size.listShown = false;
							size.listElement.style.display='none';
					}
					if (font.listShown ){
							font.listShown = false;
							font.listElement.style.display='none';
					}
				},
				updateFont: function(){
					var self = window.textTool;
					var fontDefinition = '';
					var styleType = self.style;

					if ( styleType['bold'] )
						fontDefinition += 'bold ';
					if ( styleType['cursive'] )
						fontDefinition += 'italic ';

					fontDefinition += self.size.selected.toString() + 'px ' + self.font.selected;
					ctx.font = fontDefinition;
					offscreenCtx.font = fontDefinition; 
					window.text.calculateBox();

				}
}

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - ctxMenu																//
//		Context Manu handler from its initial position to right click actions like	//
//		editcolor window showing .													//
//////////////////////////////////////////////////////////////////////////////////////
var ctxMenu = {display:false,
				element: null,
				eventshot: function(e){
					if ( !bitmap.active ){
						var element = trigger(e);
						//	CONTEXT MENU
						if ( element.id === "lienzo" && tools.selected < 2 ){
								ctxMenu.element.style.left = e.clientX+"px";
								ctxMenu.element.style.top = e.clientY+"px";
								ctxMenu.element.style.display = "block";
								ctxMenu.display = true;
						}else{
							if ( element.id.length < 3 ){	
								changeColor(element.style.backgroundColor,!canvasMouse.right);

								//DOUBLE RIGHT CLICK HANDLE
									var time = new Date();
									if ( time - window.editColors.rihtBtnTime < 350 ){
										window.editColors.clrBrushSel.element = element;
										window.editColors.clrBrushSel.rightBtn = true;
										window.editColors.showToggle();
									}
									window.editColors.rihtBtnTime = time;
							}
						}
						return window.preventDefault(e);
					}	
				},
				hide:function(){
					this.display = false;
					this.element.style.display = "none";
				}
			};



//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - windowMsg																//
//		Object to handle the message modal window to make info or error warnings.	//
//////////////////////////////////////////////////////////////////////////////////////
var windowMsg = {
				windowElement:null,
				iconElement:null,
				msgElement:null,
				closeBtn:null,
				levels:('info','error'),
				btns: [null,null,null],
				callbacks:[0,0,0],
				shown: false,
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				// When SHOW is called there are some important parameters: 											//
				// 	- level 	-> Is the type message selector, 0 to ERROR and 1 to INFO icon.							//
				//	- msg 		-> String with the message to show into the modal window.								//
				//	- btns 		-> Array with the titles (strings) of the options buttons offered.						//
				//	- callbacks -> Array with a callback function for every button into the previous parameter.			//
				//	- close 	-> Boolean to set disable (false) or enable (true) the close button of the modal window.//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				show: function (level,msg,btns,callbacks,close){
						var self = window.windowMsg;
						lvl = level;
						mes = msg || '';
						buttons = btns || ['OK'];
						callbk = callbacks || [self.hide];
						numBtns = (buttons.length > 3) ? 3 : buttons.length;
						closeState = close || false; 

						document.getElementById("confirm_button_container").style.width = (numBtns*90)+'px';

						//	SET MSG TEXT
						self.msgElement.innerHTML = mes;
						// SET MSG ICON
						if ( lvl == 1 ){
							self.iconElement.classList.add('info');
							self.iconElement.classList.remove('error');
							sounds.chord.play();
						}else{
							self.iconElement.className.replace('info');
							self.iconElement.classList.add('error');
							sounds.error.play();
						}
						// Close button 
						if ( closeState && self.closeBtn.className.match(/disabled/)){
							self.closeBtn.classList.remove('disabled');
						}else if ( !closeState && !self.closeBtn.className.match(/disabled/) ){
							self.closeBtn.classList.add('disabled');
						}
						// Action Buttons & Callbacks
						for ( var i=0; i < numBtns; i++){
							var childBtn = get_firstchild(self.btns[i]);
							childBtn.innerHTML = buttons[i];
							self.callbacks[i] = callbk[i];
							self.btns[i].style.display = 'block';
						}

						// SHOW THE WINDOW
						self.windowElement.style.display='block';
						document.getElementById('app_blocker').style.zIndex = '900';
						document.getElementById('app_blocker').style.display = 'block';
						self.shown = true;	
				},
				reset: function(){
					var self = window.windowMsg;
					for ( var i = 0; i<3; i++){
						self.btns[i].style.display = 'none';
						self.callbacks[i] = 0;
					}
				},
				hide: function(btn){
						var self = window.windowMsg;
						if ( btn === 0 && self.callbacks[0])
							self.callbacks[0]();
						if ( btn === 1 && self.callbacks[1])
							self.callbacks[1]();
						if ( btn === 2 && self.callbacks[2])
							self.callbacks[2]();		
						
						if ( self.shown ){
							self.windowElement.style.display='none';
							self.msgElement.innerHTML = '';
							document.getElementById('app_blocker').style.display = 'none';
							self.shown = false;

							self.reset();
						}	
				}	

}

//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - bitmap 																//
//		Bitmap is not a modal window but a view instead.					 		//
//		Show and image with the canvas drawing over all app to see how the painting //
//		is working, so you can download as image.									//
////////////////////////////////////////////////////////////////////////////////////// 
var bitmap = {
	element:null,
	img:null,
	active:false,
	update: function(){
		var screenDim = window.viewPortDim();
		var marginTop = (screenDim.height - canvas.height)/2;
		window.bitmap.element.style.height = screenDim.height.toString() +'px';
		window.bitmap.img.style.marginTop = marginTop.toString() + 'px';
		window.bitmap.img.src = canvas.toDataURL();
	},
	showToggle:function(e){
		if ( !rightBtn(e)){
			if ( window.bitmap.active ){
				window.bitmap.element.style.display = 'none';
				window.bitmap.active = false;
			}else{
				window.bitmap.update();
				window.bitmap.element.style.display = 'block';
				window.bitmap.active = true;
			}
		}
	}
}


//////////////////////////////////// END-[MODAL WINDOW OBJECTS]	////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%COLOR 	OPERATIONS								///
///																					///
///////////////////////////////////////////////////////////////////////////////////////	
function inverseColor(color){
	var aColor = colorToIntArray(color);
	invColor = 'rgb(';
	for ( var i=0; i<3; i++ ){
		invColor += (255 - parseInt(aColor[i])).toString()+',';
	}
	return invColor.substr(0,invColor.length-1)+')';
}	
																		
function colorToIntArray (color){

	var sColor = color.replace("rgb(","");
	sColor = sColor.replace("rgba(","");
	sColor = sColor.replace(" ","");
	sColor = sColor.replace(")","");
	aColor = sColor.split(",");
	
	return	[parseInt(aColor[0]),parseInt(aColor[1]),parseInt(aColor[2])];
}

function colorPixel(data,p,colorToFill){
	var width = canvasImageData.width;
	var color = colorToIntArray (colorToFill);;

	data[p] = color[0];	
	data[p+1] = color[1];	
	data[p+2] = color[2];	
	data[p+3] = 255;
}

function  matchStartColor(data,p,color){
	return ( data[p] === color[0] &&  data[p+1] === color[1] &&  data[p+2] === color[2] );
}

function fillWithColor(data,matchColor,X,Y,colorToFill){
	//Based in William Malone Code || http://www.williammalone.com/articles/html5-canvas-javascript-paint-bucket-tool/
	document.getElementById('app_blocker').style.display = 'block';
	var colorTest = colorToIntArray (colorToFill);
	if ( matchColor[0] != colorTest[0] ||  matchColor[1] != colorTest[1] ||  matchColor[2] != colorTest[2] ){
	
		var length = data.length;
		var width = canvasImageData.width;
		var height = canvasImageData.height;
		
	
		//Pushing start pixel into the pixels stack
		var pixelStack = [[Math.floor(X),Math.floor(Y)]];
		
		while(pixelStack.length){
			var newPos = pixelStack.pop();
			var x = newPos[0];
			var y = newPos[1];
			reachLeft = false;
			reachRight = false;
	
			// Going to the upper position		
			var pixelPos = (y*width + x) * 4;
			while(y-- >= 0 && matchStartColor(data,pixelPos,matchColor)){
				pixelPos -= width*4;
			}
	
			pixelPos += width*4;
			++y;
			
			// Go down filling pixels and evaluating every left and right pixel step by step		
			while(y++ < height-1 && matchStartColor(data,pixelPos,matchColor)){
				
				// Fill pixel width color
				colorPixel(data,pixelPos,colorToFill);
			
				if ( x > 0 ){
					if ( matchStartColor(data,pixelPos - 4,matchColor) ){
						if ( !reachLeft ){
							pixelStack.push([x-1,y]);
							reachLeft = true;	
						}
					}else if ( reachLeft ){
						reachLeft = false;	
					}
				}
				if ( x < width-1 ){
					if ( matchStartColor(data,pixelPos + 4,matchColor) ){
						if ( !reachRight ){
							pixelStack.push([x+1,y]);
							reachRight = true;	
						}
					}else if ( reachRight ){
						reachRight = false;	
					}
				}
				
				pixelPos += width*4;
			}
		}
		ctx.putImageData(canvasImageData,0,0);
	}
	tools.filling = false;
	document.getElementById('app_blocker').style.display = 'none';
}
//////////////////////////////////// END-[COLOR OPERATIONS] ////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%DRAW OPERATIONS								///
///																					///
///////////////////////////////////////////////////////////////////////////////////////	


	//////////////////////////
	//	%SHAPING FILTERING	//
	//////////////////////////
	function drawClean(coef){
		document.getElementById('app_blocker').style.display = 'block';
		var offscreenImageData = window.offscreenCtx.getImageData(0,0,offscreenCanvas.width,offscreenCanvas.height);
		var offscreenData = offscreenImageData.data;
		var length = offscreenData.length;
		var limit = coef || 127;
		
		for ( var i=3; i<length ; i+=4 ){
			if( offscreenData[i] > limit ){	// If it´s opaque
				offscreenData[i] = 255;
			}else{
				offscreenData[i] = 0;
			}
		}
		window.offscreenCtx.putImageData(offscreenImageData,0,0);
		window.ctx.drawImage(window.offscreenCanvas,0,0);
		window.offscreenCtx.clearRect(0,0,800,500);
		document.getElementById('app_blocker').style.display = 'none';
	}

	//////////////////////
	//	%BRUSH PAINTING	//
	////////////////////// 
	function brush(x,y){
		var w = 0;

		ctx.save();
		
		if ( tools.brushType>0 && tools.brushType<3 && tools.selected!=2 ){		// CIRCLE		
			
			switch (tools.brushType){
				case 2:;
					ctx.fillRect(x-1,y-2,2,4);
					ctx.fillRect(x-2,y-1,4,2);
					break;
				case 1:
					ctx.fillRect(x-3,y-1,7,3);
					ctx.fillRect(x-2,y-2,5,5);
					ctx.fillRect(x-1,y-3,3,7);
					break;
			}	
		}
		
		if ( tools.brushType===3 && tools.selected!=2 ){	// POINT
			ctx.fillRect(x,y,1,1);
		}
		
		if ( tools.brushType>3 && tools.brushType<7 || tools.selected===2 ){		// SQUARE		
			if(tools.selected===2){
				w = tools.eraserDim;
				wm = w/2;		
			}else{
				switch (tools.brushType){
					case 6:
						w=2;
						wm=1
						break;
					case 5:
						w=5;
						wm=3;
						break;
					case 4:
						w=8;
						wm=4;
						break;
				}	
			}
			ctx.fillRect(x-wm,y-wm,w,w);	
		}
		
		if ( tools.brushType>6 && tools.brushType<10 && tools.selected!=2 ){		// 45º LINE		

			switch (tools.brushType){
				case 9:
					ctx.fillRect(x-1,y,1,1);
					ctx.fillRect(x,y-1,1,1);
					break;
				case 7:
					ctx.fillRect(x-3,y+3,1,1);
					ctx.fillRect(x+3,y-3,1,1);
				case 8:
					ctx.fillRect(x-2,y+2,1,1);
					ctx.fillRect(x-1,y+1,1,1);
					ctx.fillRect(x,y,1,1);
					ctx.fillRect(x+1,y-1,1,1);
					ctx.fillRect(x+2,y-2,1,1);				
					break;
			}
		}

		if ( tools.brushType>9 && tools.brushType<13 && tools.selected!=2 ) {		// -45º LINE		
			
			switch (tools.brushType){
				case 12:
					ctx.fillRect(x-1,y-1,1,1);
					ctx.fillRect(x,y,1,1);
					break;
				case 10:
					ctx.fillRect(x-3,y-3,1,1);
					ctx.fillRect(x+3,y+3,1,1);
				case 11:
					ctx.fillRect(x-2,y-2,1,1);
					ctx.fillRect(x-1,y-1,1,1);
					ctx.fillRect(x,y,1,1);
					ctx.fillRect(x+1,y+1,1,1);
					ctx.fillRect(x+2,y+2,1,1);
					break;
			}
		}	
		ctx.restore();
	}

	//////////////////////
	//	%TRAYECTORY 	//
	////////////////////// 
	function drawTrayectory(x1,y1,x2,y2,drawShape){

		if( x1 === x2 && y1 === y2 ){
			drawShape(x1,y1);	
		}else{
		
			var deltaX = x2 - x1;
			var deltaY = y2 - y1;
		
			var divis = Math.abs(deltaX)>Math.abs(deltaY) ? deltaX : deltaY;
				divis = Math.abs(divis);
			var	incX = deltaX/divis;
			var	incY = deltaY/divis; 
			
			var prevX = 0, prevY = 0;
			for(var i=0;i<divis;i+=1){	

				var calcX = Math.round(x1+incX*i);
				var calcY = Math.round(y1+incY*i);

				drawShape(calcX,calcY);

				// For the brush painting
				if ( drawShape === brush && tools.brushType!==3)

					drawShape(calcX+1,calcY);
			}
		}
	}

		function fillPixel(x,y,size){
				ctx.fillRect(x,y,size,size);
		}

		function invertImageColor(){
			document.getElementById('app_blocker').style.display = 'block';
			if ( window.selectionFrame.cutted.is ){
				window.selectionFrame.invertSelectionColor();
			}else{
				//CURVES
					tools.curveState=0;
				//POLIGON
					if ( tools.selected === 13 && tools.poligon.points.length > 1){
						tools.poligon.close();
					}	
				// TEXT
					if ( tools.selected === 9 && window.text.active){
						window.text.deactivate();
					} 
				window.offscreenCtx.drawImage(window.canvas,0,0,canvas.width,canvas.height);
				window.drawInverseColor();
				window.ctx.drawImage(window.offscreenCanvas,0,0,canvas.width,canvas.height);
				window.saveImage();
			}	
			document.getElementById('app_blocker').style.display = 'none';
		}

		function drawInverseColor(){
			document.getElementById('app_blocker').style.display = 'block';
			var oSImgData = offscreenCtx.getImageData(0,0,canvas.width,canvas.height);
			var data = oSImgData.data;
			var canvasData = window.canvasImageData.data;
			for (var i=0, end=data.length; i<end; i+=4){
				if ( data[i+3] > 0 ){
					data[i] = 255 - canvasData[i];
					data[i+1] = 255 - canvasData[i+1];
					data[i+2] = 255 - canvasData[i+2];
				}
			}
			offscreenCtx.putImageData(oSImgData,0,0);
			document.getElementById('app_blocker').style.display = 'none';
		}
		function drawResizeColor(){
			var oSImgData = offscreenCtx.getImageData(0,0,canvas.width,canvas.height);
			var data = oSImgData.data;
			var canvasData = window.canvasImageData.data;
			document.getElementById('app_blocker').style.display = 'block';
			for (var i=0, end=data.length; i<end; i+=4){
				if ( data[i+3] > 0 ){
					if ( data[i] < 255 ){
						data[i] = 255 - canvasData[i];
						data[i+1] = 255 - canvasData[i+1];
						data[i+2] = 255 - canvasData[i+2];
					}else{
						data[i] = canvasData[i];
						data[i+1] = canvasData[i+1];
						data[i+2] = canvasData[i+2];
					}
				}
			}
			offscreenCtx.putImageData(oSImgData,0,0);
			document.getElementById('app_blocker').style.display = 'none';
		}	

		function drawRect(XINI,YINI,DELTAX,DELTAY){

			var lineWidth = ctx.lineWidth;
			var haveToStroke = tools.rectangle.stroke;
			var haveToFill = tools.rectangle.fill; 

			if ( haveToFill ){
				ctx.fillStyle = canvasMouse.right*!haveToStroke + !canvasMouse.right*haveToStroke ? tools.secColor : tools.primColor;
				ctx.fillRect(XINI,YINI,DELTAX,DELTAY);
			}

			if ( haveToStroke ){
				ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
				//Up
				ctx.fillRect(XINI,YINI,DELTAX,lineWidth);
				//Down
				ctx.fillRect(XINI,YINI+DELTAY-lineWidth,DELTAX,lineWidth);
				//Left
				ctx.fillRect(XINI,YINI,lineWidth,DELTAY);
				//Right
				ctx.fillRect(XINI+DELTAX-lineWidth,YINI,lineWidth,DELTAY);
			}
		}

		function drawCircle(XINI,YINI,DELTAX,DELTAY){

			var Cx = DELTAX/2;
			var	Cy = DELTAY/2;
			var radius = DELTAX>DELTAY ? Cx : Cy;
			var pr = radius*radius;
			var centerX = XINI + Cx;
			var centerY = YINI + Cy;
			var size = tools.lineWidth;
			var sm = size > 1 ? size/2 : 0;
			var haveToFill = tools.circle.fill;
			var haveToStroke = tools.circle.stroke;
			var offscreenImageData = {};

		  		var relX = DELTAX/DELTAY;
		  		var relY = DELTAY/DELTAX;

		  		offscreenCtx.clearRect(0,0,canvas.width,canvas.height);


				// CIRCLE FILL
				if ( haveToFill ){
					var cFillY = centerY;
					var cFillX = centerX;
					offscreenCtx.save();
				  		
						if(DELTAX>DELTAY){
							var relFill = Math.abs(DELTAY/DELTAX);
							offscreenCtx.scale(1,relFill);
							cFillY /= relFill;
							
						}else if(DELTAX<DELTAY){
							var relFill = Math.abs(DELTAX/DELTAY);
							offscreenCtx.scale(relFill,1);
							cFillX /= relFill;
						}	
						
						offscreenCtx.beginPath();
						offscreenCtx.arc(cFillX,cFillY,Math.abs(radius),0,Math.PI*2,true);

						offscreenCtx.fillStyle = canvasMouse.right*!haveToStroke + !canvasMouse.right*haveToStroke ? tools.secColor : tools.primColor;
						offscreenCtx.fill();				
					offscreenCtx.restore();	
					drawClean();
				}

				// CIRCLE STROKE
				ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
				
				for ( var X=-DELTAX,end=DELTAX+1; X<end; X++ ){
					var Xprima = X*relY;
					var Y = Math.round(Math.sqrt(Cy*Cy-Xprima*Xprima));

					ctx.fillRect(Math.floor(centerX+X)-sm,Math.floor(centerY+Y)-sm,size,size);
					ctx.fillRect(Math.floor(centerX+X)-sm,Math.floor(centerY-Y)-sm,size,size);
				}
				for ( var Y=-DELTAY,end=DELTAY+1; Y<end; Y++ ){
					var Yprima = Y*relX;
					var X = Math.round(Math.sqrt(Cx*Cx-Yprima*Yprima));

					ctx.fillRect(Math.floor(centerX+X)-sm,Math.floor(centerY+Y)-sm,size,size);
					ctx.fillRect(Math.floor(centerX-X)-sm,Math.floor(centerY+Y)-sm,size,size);
				}
					  
		}

		function drawRoundRectangle(XINI,YINI,width,height){
			var XEND = XINI + width;
			var YEND = YINI + height;
			var MINW = Math.abs(width)*2;
			var MINH =  Math.abs(height)*2;
			var r = tools.roundRad;
			var haveToFill = tools.rounded.fill;
			var haveToStroke = tools.rounded.stroke;
			var rect = tools.lineWidth > 1 ? 0 : 0.5; // Corecction for 1px lines
			
			//Radius rectification
			if ( Math.abs(width) >= 2*r ){
				if ( width > 0 )
					disX = r;
				else
					disX = -r;	
			}else{
				disX = width/2;	
			}
			
			if ( Math.abs(height) >= 2*r ){
				if ( height > 0 )
					disY = r;
				else
					disY = -r;	
			}else{
				disY = height/2;	
			}
			
			offscreenCtx.beginPath();
			
			offscreenCtx.moveTo(XINI+disX,YINI+rect);
			offscreenCtx.lineTo(XEND-disX,YINI+rect);
			offscreenCtx.quadraticCurveTo(XEND+rect,YINI+rect,XEND+rect,YINI+disY);
			offscreenCtx.lineTo(XEND+rect,YEND-disY);
			offscreenCtx.quadraticCurveTo(XEND+rect,YEND+rect,XEND-disX,YEND+rect);
			offscreenCtx.lineTo(XINI+disX,YEND+rect);
			offscreenCtx.quadraticCurveTo(XINI+rect,YEND+rect,XINI+rect,YEND-disY);
			offscreenCtx.lineTo(XINI+rect,YINI+disY);
			offscreenCtx.quadraticCurveTo(XINI+rect,YINI+rect,XINI+disX,YINI+rect);

			if ( haveToFill ){
				offscreenCtx.fillStyle = canvasMouse.right*!haveToStroke + !canvasMouse.right*haveToStroke ? tools.secColor : tools.primColor;
				offscreenCtx.fill();
				drawClean(127);
			}

			offscreenCtx.strokeStyle = canvasMouse.right ? tools.secColor : tools.primColor;
			offscreenCtx.stroke();
			drawClean(127);
			
		}


//////////////////////
//	%MAIN DRAWING	//
////////////////////// 
function draw(){
		ctx.lineWidth = tools.lineWidth;
		offscreenCtx.lineWidth = tools.lineWidth;

		if ( tools.selected < 2){	//SELECTION
			var selFrame = window.selectionFrame;	
			if ( selFrame.active ){
				ctx.putImageData(canvasImageData,0,0);
				selFrame.draw();
			}	
		}		

		switch (tools.selected){
				
			case 2:	//ERASER
					var halfRect = Math.round(tools.eraserDim/2);
					var color = !canvasMouse.right ? tools.secColor : tools.primColor;
					ctx.putImageData(canvasImageData,0,0);
		
					if(canvasMouse.keypress){	
						ctx.fillStyle = color;
						drawTrayectory(tools.initX,tools.initY,canvasMouse.x,canvasMouse.y,brush);		
						
						tools.initX = canvasMouse.x;
						tools.initY = canvasMouse.y;
						canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
					}else{

						ctx.fillStyle = "rgba(0,0,0,255)";
						ctx.fillRect(canvasMouse.x - halfRect, canvasMouse.y - halfRect, tools.eraserDim, tools.eraserDim);
						ctx.fillStyle = tools.secColor;
						ctx.fillRect(canvasMouse.x - halfRect+1, canvasMouse.y - halfRect+1, tools.eraserDim-2, tools.eraserDim-2);
					}
				break;
					
			case 4:	//	PICK COLOR
				if(canvasMouse.keypress){
					ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
							
					var pixel = ctx.getImageData(tools.initX,tools.initY,1,1).data;
					var color = pixel[3] ? "rgb("+pixel[0]+","+pixel[1]+","+pixel[2]+")" : "white";

					document.getElementById("options_container").style.backgroundColor = color;
					tools.initX = canvasMouse.x;
					tools.initY = canvasMouse.y;
				}
			break;
			case 5: //	MAGNIFIER
				if ( !tools.magnifier.is && canvasMouse.inside ){
					var mag = window.tools.magnifier;
					ctx.putImageData(canvasImageData,0,0);
					offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
					var x1 = canvasMouse.x-mag.zoomWin.wm;
					var y1 = canvasMouse.y-mag.zoomWin.hm;
					var w = mag.zoomWin.w+1;
					var h = mag.zoomWin.h+1;
					if ( canvas.width < w ){
						w = canvas.width;
						x1 = 0;
					}
					if ( canvas.height < h ){
						h = canvas.height;
						y1 = 0;
					}
						offscreenCtx.fillRect(x1,y1,w,1);
						offscreenCtx.fillRect(x1,y1+h,w,1);
						offscreenCtx.fillRect(x1,y1,1,h+1);
						offscreenCtx.fillRect(x1+w,y1,1,h+1);
					window.drawInverseColor();
					ctx.drawImage(offscreenCanvas,0,0,canvas.width,canvas.height);
				}	
				break;
			case 6:	//	PENCIL
				if(canvasMouse.keypress){
					ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
					drawTrayectory(tools.initX,tools.initY,canvasMouse.x,canvasMouse.y,function(x,y){ctx.fillRect(x,y,1,1);});		
					
					tools.initX = canvasMouse.x;
					tools.initY = canvasMouse.y;
				}
			break;
			
			case 7:	//	BRUSH
				ctx.putImageData(canvasImageData,0,0);
				
				if(canvasMouse.keypress){
					ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;	
					drawTrayectory(tools.initX,tools.initY,canvasMouse.x,canvasMouse.y,brush);		
					
					tools.initX = canvasMouse.x;
					tools.initY = canvasMouse.y;
					canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
				}else{
					ctx.fillStyle = tools.primColor;
					brush(canvasMouse.x,canvasMouse.y);
				}	
			break;	
			
			case 8:	//	AIRBRUSH
				
				if(canvasMouse.keypress && !tools.airbrushInterv){
					ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
					tools.airbrushInterv = setInterval(function(){				
						var r = Math.random()*tools.airbrushRad;
						var theta = Math.random()*2*Math.PI;
						
						ctx.fillRect(canvasMouse.x+Math.round(r*Math.cos(theta)),canvasMouse.y+Math.round(r*Math.sin(theta)),1,1);	
					},5);
				}	
			break;
			
			case 9: //	TEXT
					if(window.text.active){
						ctx.putImageData(canvasImageData,0,0);
						text.draw();
					}
				break;
			
			case 10:	//	LINE
				if(canvasMouse.keypress){	
					ctx.putImageData(canvasImageData,0,0);

					ctx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;
					drawTrayectory(tools.initX,tools.initY,canvasMouse.x,canvasMouse.y,function(x,y){fillPixel(x,y,tools.lineWidth);});

					updateDimBox(canvasMouse.x - tools.initX,canvasMouse.y - tools.initY);
				}
			break;
			
			case 11:	//	CURVE
				if(canvasMouse.keypress){	
					
					offscreenCtx.strokeStyle = canvasMouse.right ? tools.secColor : tools.primColor;
					offscreenCtx.lineWidth = tools.lineWidth;
					if(tools.curveState==0){
						tools.anchPntX = canvasMouse.x;
						tools.anchPntY = canvasMouse.y;	
						tools.ctrlPntX[0] = tools.initX;
						tools.ctrlPntY[0] = tools.initY;
						tools.ctrlPntX[1] = canvasMouse.x;
						tools.ctrlPntY[1] = canvasMouse.y;
					}else if(tools.curveState==1){
						tools.ctrlPntX[0] = canvasMouse.x;
						tools.ctrlPntY[0] = canvasMouse.y;
					}else if(tools.curveState==2){
						tools.ctrlPntX[1] = canvasMouse.x;
						tools.ctrlPntY[1] = canvasMouse.y;
					}
									
					ctx.putImageData(canvasImageData,0,0);
					

					offscreenCtx.beginPath();
					
					offscreenCtx.moveTo(tools.initX,tools.initY);
					offscreenCtx.bezierCurveTo(tools.ctrlPntX[0],tools.ctrlPntY[0],tools.ctrlPntX[1],tools.ctrlPntY[1],tools.anchPntX,tools.anchPntY);
					
					offscreenCtx.stroke();

					drawClean();

					updateDimBox(canvasMouse.x - tools.initX,canvasMouse.y - tools.initY);
				}
			break;
			
	}
	// For any movement
	if (canvasMouse.x !== tools.initX && canvasMouse.y !== tools.initY ){

		if ( tools.selected === 12 || tools.selected === 14 || tools.selected ===15 ){
			if(canvasMouse.keypress){
						
				  ctx.putImageData(canvasImageData,0,0);
				  


					var halfLineWidth = tools.lineWidth != 1 ? tools.lineWidth/2 : 0;
					  var XINI,YINI,XEND,YEND;
					  
					  //Dimension rectification because lineWidth
					  if ( deltaX < 0 ){ 
						  XINI = tools.initX - halfLineWidth;	
						  XEND = canvasMouse.x + halfLineWidth;
					  }else{
						  XINI = tools.initX + halfLineWidth;	
						  XEND = canvasMouse.x - halfLineWidth;
					  }
					 
					  if ( deltaY < 0 ){
						  YEND = canvasMouse.y + halfLineWidth;
						  YINI = tools.initY - halfLineWidth;
					  }else{
						  YEND = canvasMouse.y - halfLineWidth;
						  YINI = tools.initY + halfLineWidth;
					  }


				  var deltaX = XEND - XINI;
				  var deltaY = YEND - YINI;

				  // Updating values of footer info bar
				  updateDimBox(deltaX,deltaY);

				  if ( deltaX < 0 ){
				  	XINI = canvasMouse.x;
				  	deltaX *= -1; 	
				  }
				  if ( deltaY < 0 ){
				  	YINI = canvasMouse.y;
				  	deltaY *= -1; 
				  }
				 
				 /////////////////////////////////////////
				 // SAME PROPORTIONS WHEN SHIFT IS PRESSED
				  if ( keyboard.shift ){
				  	 if ( deltaX < deltaY ){
				  	 	deltaY = deltaX;
				  	 }else{
				  	 	deltaX = deltaY;
				  	 }
				  }
				  /////////////////////////////////////////
				  
				  
				 if ( tools.selected === 12 ){	//RECTANGLE
				 	drawRect(XINI,YINI,deltaX,deltaY);
				 }

				 if ( tools.selected === 14 ){	//CIRCLE
				 	if ( deltaX === 0 ){
				 		drawTrayectory(XINI,YINI,XINI,YEND,function(x,y){fillPixel(x,y,tools.lineWidth);});
				 	}else if ( deltaY === 0 ){
				 		drawTrayectory(XEND,YINI,XEND,YINI,function(x,y){fillPixel(x,y,tools.lineWidth);});
				 	}else{
				 		drawCircle(XINI,YINI,deltaX,deltaY);
				 	}	
				 }

				if ( tools.selected === 15 ){	//Round rectangle
					drawRoundRectangle(XINI,YINI,deltaX,deltaY);
				}
				
			}	
		}	

		if ( tools.selected === 13 && tools.poligon.points.length > 0){	//POLIGON
		  	var polPoints = tools.poligon.points;
		  	var numPoints = polPoints.length;
				
			// Clean both canvas	
			ctx.putImageData(canvasImageData,0,0);
			offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
			
			// Set the color		  	
			offscreenCtx.fillStyle = canvasMouse.right ? tools.secColor : tools.primColor;

			// Draw lines between the stored points
			tools.poligon.draw(function(x,y){tools.poligon.fillPixel(x,y,offscreenCtx);});

			// Draw the active line to set if the mouse is pressed
			if(canvasMouse.keypress){			
				drawTrayectory(polPoints[numPoints-1].x,polPoints[numPoints-1].y,canvasMouse.x,canvasMouse.y,function(x,y){tools.poligon.fillPixel(x,y,offscreenCtx);});
			}

			// For 'only fill' case, it makes the color inversion into the stroke drawn
			if ( !tools.poligon.stroke && tools.poligon.fill ){
				window.drawInverseColor();
			}
			ctx.drawImage(offscreenCanvas,0,0);

			updateDimBox(canvasMouse.x - tools.initX,canvasMouse.y - tools.initY);
		}
	}
	
}
//////////////////////////////////// END-[DRAW OPERATIONS] ////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%EVENT HANDLERS									///
///																					///
///////////////////////////////////////////////////////////////////////////////////////	

//////////////////////////////
//		%MOUSE EVENTS		//
//////////////////////////////
function updateDimBox(deltaX,deltaY){
	var X,Y;
	
	if ( deltaX === 0 )	X = 1; 	else	X = deltaX;
	if ( deltaY === 0 )	Y = 1; 	else	Y = deltaY;

	document.getElementById("dim_box").value = X+"x"+Y;	
}

function selecCursorChanger(){
	var pointSelection = window.selectionFrame.isOverPntSelec(canvasMouse.x,canvasMouse.y);
	window.canvas.className = "left "+window.canvasMouse.cursor;
	
	window.selectionFrame.pointOver = pointSelection;
	
	if ( pointSelection ){		
		if ( pointSelection == 'n' || pointSelection == 's' ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_ns");		
		}else if ( pointSelection == 'w' || pointSelection == 'e' ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_ew");
		}else if ( pointSelection == 'nw' || pointSelection == 'se' ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_nwse");
		}else if ( pointSelection == 'ne' || pointSelection == 'sw' ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_nesw");
		}
	}else if ( selectionFrame.isOverBorder(canvasMouse.x,canvasMouse.y) || selectionFrame.moving){
		if ( tools.selected === 9 ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_arrow");
		}else{
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_move");
		}	
	}else if (selectionFrame.isOver(canvasMouse.x,canvasMouse.y)){
		if ( tools.selected === 9 ){
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_text");
		}else{
			canvas.className = canvas.className.replace(canvasMouse.cursor,"c_move");
		}
	}
}

function coordUpdate(e){	
	
	var coordMouse = coordIntoCanvas(canvas,e);
	//WINDOWS DRAGGING
	if ( window.windowsDragging.bClicked ){
		window.windowsDragging.mousemove(e);
	}

	// CANVAS REDIMENSION
	if ( window.resizeCanvas.active ){
		window.resizeCanvas.move(e);
	}

	// TEXT
		if ( text.contentSelected.clicked ){
			text.selectTextMove(canvasMouse.x,canvasMouse.y);
			window.edit.enable('clear');
			window.edit.enable('copy');
			window.edit.enable('cut');
		}

	///////////////////////////////////////////////////////////////	
	// SYSTEM COORDS UPDATING
		canvasMouse.x =coordMouse.x;
		canvasMouse.y = coordMouse.y;
		if ( selectionFrame.sizing || selectionFrame.moving || selectionFrame.cutZone ){
			selectionFrame.update(canvasMouse.x,canvasMouse.y);
			tools.initY = canvasMouse.y;
			tools.initX = canvasMouse.x;
		}
		if ( selectionFrame.bDraw ){
			selectionFrame.update(canvasMouse.x,canvasMouse.y);
		}

	// STATUS COORDS UPDATING
	if(canvasMouse.inside){
		document.getElementById("coord_box").value = (canvasMouse.x)+","+(canvasMouse.y);
	}	
	///////////////////////////////////////////////////////////////

	// CURSOR IMGE MANAGEMENT
		if ( (tools.selected === 9 || tools.selected < 2) && !selectionFrame.sizing && selectionFrame.active){	
			selecCursorChanger();
		}
	////////////	
		draw();	//
	////////////
	return window.preventDefault(e);
}


function clickDownCanvas(e){
	var paintingPoligon = tools.selected === 13 && tools.poligon.points.length > 0;

	canvasMouse.keypress = true;		

	// CONTEXT MENU
		if ( ctxMenu.display ){
			ctxMenu.hide();
		}	
	// MOUSE COORDINATES MANAGEMENT	
		canvasMouse.right = rightBtn(e);
		coord = coordIntoCanvas(canvas,e);
		canvasMouse.x = coord.x;
		canvasMouse.y = coord.y;
	///////////////////////////////////////////////////////////////////////////////////
	// REFRESHING CANVAS IMAGE DATA 
		if ( tools.curveState === 0 && !window.selectionFrame.active && !paintingPoligon && tools.selected !== 5){
			canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
			tools.initX = coord.x;
			tools.initY = coord.y;
		}
	//////////////////////////////////////////////////////////////////////////////////
	// SELECTION
		if ( tools.selected < 2 ){
			if ( window.selectionFrame.active ){
				if( window.selectionFrame.isOverPntSelec(canvasMouse.x,canvasMouse.y) ){
					window.selectionFrame.sizing = true;
				}else if( window.selectionFrame.isOverBorder(canvasMouse.x,canvasMouse.y) ){
					window.selectionFrame.moving = true;	
				}else if ( window.selectionFrame.isOver(canvasMouse.x,canvasMouse.y) ){
					window.selectionFrame.drag = true;
					window.selectionFrame.moving = true;
				}else{
					window.selectionFrame.deactivate();
					window.edit.disable('clear');
					window.edit.disable('copyto');
					window.edit.disable('copy');
					window.edit.disable('cut');
					window.edit.disable('paste');
					saveImage();
				}
			}else{	
				window.tools.clearImage.changeState(false);
				window.selectionFrame.reset();
				window.selectionFrame.active = true;
				if ( tools.selected === 1){
					window.selectionFrame.bDraw = true;
					window.selectionFrame.x = canvasMouse.x;
					window.selectionFrame.y = canvasMouse.y;
				}else{
					window.selectionFrame.initCutting(canvasMouse.x,canvasMouse.y);
				}	
			}
			tools.initY = canvasMouse.y;
			tools.initX = canvasMouse.x;
		}	
	// FILL WITH COLOR
		if ( tools.selected === 3 && !tools.filling ){	
			var color = canvasMouse.right ? tools.secColor : tools.primColor;
			tools.filling = true;
			fillWithColor(canvasImageData.data,ctx.getImageData(canvasMouse.x, canvasMouse.y,1,1).data, canvasMouse.x, canvasMouse.y, color);
		}
	// MAGNIFIER
		if ( tools.selected === 5 ){
			window.ctx.putImageData(canvasImageData,0,0);
			canvasMouse.keyPress = false;
			if( tools.magnifier.is ){
				tools.magnifier.zoomOut();
			}else{
				tools.magnifier.zoomIn();
			}
		}	
	// TEXT
		if (window.tools.selected === 9 ){
			window.edit.disable('clear');
			window.edit.disable('copy');
			window.edit.disable('cut');
			window.edit.disable('paste');
			if ( window.text.active ){
				 if( selectionFrame.isOverPntSelec(canvasMouse.x,canvasMouse.y) ){
					tools.initY = canvasMouse.y;
					tools.initX = canvasMouse.x;
					selectionFrame.sizing = true;
				}else if( selectionFrame.isOverBorder(canvasMouse.x,canvasMouse.y) ){
					tools.initY = canvasMouse.y;
					tools.initX = canvasMouse.x;
					selectionFrame.moving = true;	
				}else if ( selectionFrame.isOver(canvasMouse.x,canvasMouse.y) ){
					if ( textTool.toolbar.selected ){
						textTool.select();
					}
					text.clickOnText(canvasMouse.x,canvasMouse.y);
				}else{
					text.deactivate();
					saveImage();
				}
			}else{
				window.text.coord.X = coord.x;
				window.text.coord.Y = coord.y;
				window.text.activate();
			}
		}
		window.textTool.hideList();
	//POLIGON
		if ( tools.selected === 13 && tools.poligon.points.length === 0){
			tools.poligon.points.push(coord);
		}

		if ( 	canvasMouse.inside
				&& tools.selected !== ''
				&& tools.selected !== 0
				&& tools.selected !== 1 
				&& tools.selected !== 4
				&& tools.selected !== 5
				&& tools.selected !== 9 
				&& tools.selected !== 13 
				&& tools.curveState === 0){
			window.tools.drawing = true;
		}
	
	draw();	//
	//////////
}

function clickUpCanvas(e){
	//WINDOWS DRAGGING
	if ( window.windowsDragging.bClicked )
		window.windowsDragging.mouseup();
	//	EDIT COLORS
		window.editColors.mousepress = false;
		window.editColors.lumiPress = false;

	// CANVAS REDIMENSION
		if ( window.resizeCanvas.active ){
			window.resizeCanvas.redimension(e);
		}
		
		if ( tools.selected === 9 && text.contentSelected.clicked ){
			text.contentSelected.clicked = false;
			selectionFrame.sizing = false;
			selectionFrame.moving = false;
		}
	// SELECTION FRAME
		if ( selectionFrame.sizing){
			selectionFrame.updateSize();
			selectionFrame.sizing = false;
		}
		if ( selectionFrame.moving ){
			selectionFrame.moving = false;
		}
		if ( selectionFrame.bDraw || selectionFrame.cutZone ){
			selectionFrame.cutImage();
			window.edit.enable('clear');
			window.edit.enable('copy');
			window.edit.enable('cut');
		}	
	// POLIGON
		if ( tools.selected === 13 && tools.poligon.points.length > 0){
			var poligon = tools.poligon;
			poligon.points.push({x:canvasMouse.x,y:canvasMouse.y});

			if ( Math.abs(canvasMouse.x - poligon.points[0].x) <= tools.lineWidth*3 
				&& Math.abs(canvasMouse.y - poligon.points[0].y) <= tools.lineWidth*3 ){
					poligon.close();
			}
		}
	// GENERAL MANAGEMENT
		if(canvasMouse.keypress){
			canvasMouse.keypress = false;	
		}
		document.getElementById("dim_box").value = "";
	// AIRBRUSH ACTIONS
		if (tools.airbrushInterv){
			clearInterval(tools.airbrushInterv);
			tools.airbrushInterv = null;
		}
	// PICK COLOR
		if (tools.selected === 4 && canvasMouse.inside ){
			var color = document.getElementById("options_container").style.backgroundColor;
			document.getElementById("options_container").style.backgroundColor = "";
			changeColor(color,!rightBtn(e));
		}
	// CURVES drawing state updating
		if( tools.selected === 11 ){
			if ( canvasMouse.inside || tools.curveState !== 0 ){
				tools.curveState = (tools.curveState<2) ? ++tools.curveState : 0;
			}
		}
	// IMAGE AUTOSAVING
		if ( 	window.tools.drawing
				&& tools.selected !== ''
				&& tools.selected !== 0
				&& tools.selected !== 1 
				&& tools.selected !== 4
				&& tools.selected !== 5
				&& tools.selected !== 9 
				&& tools.selected !== 13 
				&& tools.curveState === 0){
			window.tools.drawing = false;
			saveImage();
		}
}

function mouseOver(e){
	canvasMouse.inside=true;
}

function mouseOut(e){
	canvasMouse.inside=false;
	document.getElementById("coord_box").value = "";
	if ( tools.selected === 5 ){
		ctx.putImageData(canvasImageData,0,0);
	}
}

//////////////////////////
//		%KEY EVENTS		//
//////////////////////////

function keyDown(e){
	var e = e || window.event;
	var key = e.keyCode;
	if(keyboard.ctrl){	
		switch (key){
			case 65: 	//ctrl+A
				if ( window.tools.selected === 9 && window.text.cursor.active ){
					window.text.selectAll();
				}else {
					window.selectionFrame.selectAll();
				}	
				return window.preventDefault(e);
				break;
			case 69: 	//ctrl+E
				window.canvasAttributes.show();
				return window.preventDefault(e);
				break;	
			case 70: 	//ctrl+F
				window.bitmap.showToggle();
				return window.preventDefault(e);
				break;	
			case 77: 	//ctrl+M
				window.resetPaint();
				return window.preventDefault(e);
				break;
			case 81: 	//ctrl+Q
				window.stretchSkew.showToggle();
				return window.preventDefault(e);
				break;	
			case 82: 	//ctrl+R
				window.flipRotate.showToggle();
				return window.preventDefault(e);
				break;	
			case 83: 	//ctrl+S
				window.downloadImage();
				return window.preventDefault(e);
				break;
			case 85: 	//ctrl+U
				window.showHide('view','left');
				return window.preventDefault(e);
				break;
			case 86: 	//ctrl+V
				window.paste();
				return window.preventDefault(e);
				break;	
			case 88: 	//ctrl+X
				window.cut();
				return window.preventDefault(e);
				break;	
			case 76: 	//ctrl+L
				window.showHide('view','colors');
				return window.preventDefault(e);
				break;	
			case 67: 	//ctrl+C
				if ( keyboard.shift ){ //+ shift
					window.tools.clearImage.clear();
				}else{
					window.copy();
				}
				return window.preventDefault(e);
				break;
			case 73: 	//ctrl+I
				window.invertImageColor();
				return window.preventDefault(e);
				break;	
		}					
	}

	switch (key){
		case 16: 	//SHIFT
			keyboard.shift = true;
			break;
		case 17: 	//CTRL
			keyboard.ctrl = true;
			break;
		case 18: 	//ALT
			keyboard.alt = true;
			break;
		default:
			if ( tools.selected === 9 && text.cursor.active){
				if ( (key === 8 || (key > 36 && key < 41) ) ){
					text.addChar(key);
					return window.preventDefault(e);
				}
			}

			if ( key === 46 && window.selectionFrame.active ){ // DELETE
				window.clearSelect();
			    return window.preventDefault(e);
			}
			
			
			if( key === 27) {	//ESC
				if ( tools.selected === 13 ){	//Poligon
					window.tools.poligon.reset();
				}
				if ( tools.selected === 9 ){	//Text
					window.text.deactivate();
					window.ctx.putImageData(canvasImageData,0,0);
				}
				if ( tools.selected < 2 ){	//Select
					window.selectionFrame.deactivate();
				}
				return window.preventDefault(e);;
			}
	}
}

function keyPress(e){ 
	var e = e || window.event;	
	var key = e.keyCode || e.charCode;
	if(keyboard.ctrl){	
		if ( key === 26 || key== 122 ){ //ctrl+Z

			if ( tools.selected === 9 && text.active && text.cursor.active ){
				text.contentBack();
			}else if ( !window.selectionFrame.active ){
					stepBack();
			}
		}	

		if ( key === 25 || key== 121 ){ //ctrl+Y

			if ( !window.selectionFrame.active ){
				stepForward();
			}	
		}					
	}else
	if ( tools.selected === 9 && text.cursor.active ){

		text.addChar(key);
		return window.preventDefault(e);
	}	
}

function keyUp(e){
	var e = e || window.event;	
	switch (e.keyCode){
		case 16: 	//SHIFT
			keyboard.shift = false;
			break;
		case 17: 	//CTRL
			keyboard.ctrl = false;
			break;
		case 18: 	//ALT
			keyboard.alt = false;
			break;			
	}
	return window.preventDefault(e);
}

//////////////////////////////
//		%TOOLS EVENTS		//
//////////////////////////////
function takeToolSelected(e){
	var element = trigger(e);
	var selectedName = element.id.replace("_img","");

	if ( window.tools.magnifier.zoomMag > 1 && selectedName === 'text' ){
		window.sounds.chord.play();
	}else{
		window.selectTool(selectedName);
	}	

	return window.preventDefault(e);
}

function selectTool(toolId){
	var prevSelected = tools.selected;

	// Tool buttons class configuration
	for(var i=0;i<tools.types.length;i++){
		var tool_button = document.getElementById(tools.types[i]);
		if(tools.types[i] === toolId){	
			selected = i;
			tool_button.className = tool_button.className.replace (" outset"," inset selected");	
		}else{
			tool_button.className = tool_button.className.replace (" inset selected"," outset");
		}
	}	
	window.tools.selected = selected;

if ( prevSelected !== selected ){
	////////////////////////////////////////////////////////////////////
	// 			TOOLS RESET 		
		//CURVES
			tools.curveState=0;
		//POLIGON
			if ( prevSelected === 13 && tools.poligon.points.length > 1){
				tools.poligon.close();
			}	
		// TEXT
			if ( prevSelected === 9 ){
				if ( window.text.active ){
					window.text.deactivate();
				}
				window.tools.clearImage.changeState(true);
			} 
		//SELECT
			if ( prevSelected < 2 &&  window.selectionFrame.active ){
				window.selectionFrame.deactivate(prevSelected+1);
				saveImage();
			}
		// TOOL options RESET
			var options_frame = document.getElementsByClassName("options_frame");
			for(var i=0;i<options_frame.length;i++){
				options_frame[i].style.display="none";
			}
	//////////////////////////////////	
	//			OPTION PANEL MANAGEMENT
		if(selected<2 || selected>8){
			canvas.className = "left c_default";
			canvasMouse.cursor = "c_default";
			if ( selected === 9 ){	// TEXT
				window.tools.clearImage.changeState(false);
			}
			if( selected === 10 || selected === 11 ){	// LINE, CURVE
				document.getElementById("line_options").style.display="block";	
			}
			if( selected<2 || selected === 9 ){	// SELECT FORMS & TEXT
				document.getElementById("fill_options").style.display="block";	
			}
			if( selected === 12 ){	// RECTANGLES
				document.getElementById("rectangle_options").style.display="block";		
			}
			if( selected === 13 ){	// POLIGON
				document.getElementById("poligon_options").style.display="block";		
			}
			if( selected === 14 ){	// CIRLCE
				document.getElementById("circle_options").style.display="block";		
			}
			if( selected === 15 ){	// ROUNDED RECTANGLES
				document.getElementById("rounded_options").style.display="block";		
			}
		}else if( selected === 2 ){	// ERASER
			canvas.className = "left c_default";
			canvasMouse.cursor = "c_default";
			document.getElementById("eraser_options").style.display="block";		
		}else if( selected === 3 ){	// FILL COLOR
			canvas.className = "left c_fill_with_color";
			canvasMouse.cursor = "c_fill_with_color";	
		}else if( selected === 4 ){	// PICK COLOR
			canvas.className = "left c_pick_color";	
			canvasMouse.cursor = "c_pick_color";	
		}else if( selected === 5 ){	// MAGNIFIER
			tools.magnifier.lastTool = (prevSelected === "") ? 'pencil' : tools.types[prevSelected];
			canvas.className = "left c_magnifier";
			canvasMouse.cursor = "c_magnifier";	
			document.getElementById("magnifier_options").style.display="block";	
		}else if( selected === 6 ){	// PENCIL
			canvas.className = "left c_pencil";	
			canvasMouse.cursor = "c_pencil";
		}else if( selected === 7 ){	// BRUSH
			canvas.className = "left c_brush";
			canvasMouse.cursor = "c_brush";
			document.getElementById("brush_options").style.display = "block";	
		}else if( selected === 8 ){	// AIRBRUSH
			canvas.className = "left c_airbrush";
			canvasMouse.cursor = "c_airbrush";
			document.getElementById("airbrush_options").style.display = "block";	
		}
		canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
	}
}

function optionClasesChanger(element){
	var optionsElements = element.parentNode.childNodes;

	for ( var i=0,end=optionsElements.length; i<end; i++ ){
		var opEl = optionsElements[i];
		if (opEl.nodeName.toUpperCase() === 'DIV' && opEl.className.indexOf('selected') !== (-1) ){
			opEl.className = opEl.className.replace(' selected','');
		}
	}
	element.className += ' selected';
}

function selectOption(e){
	var element = trigger(e);
	var option = element.id.split("_option");
	
	optionClasesChanger(element);
	
	switch (option[0]){
		case "fill":
			tools.backgroundTransp = (option[1] === '1') ? false : true ;
			window.tools.changeBackgroundTransp();
			if ( tools.selected === 9){
				window.text.draw();
			}
			break;	
		case "eraser":
			tools.eraserDim = (parseInt(option[1])+1)*2;
			
			break;
		case "magnifier":
			tools.magnifier.currentMag = parseInt(option[1])>2 ? parseInt(option[1])*2 : parseInt(option[1]);
			tools.magnifier.calcZoomWin();
			tools.magnifier.zoomIn();
			break;
		case "brush":
			tools.brushType = parseInt(option[1]);
			break;
		case "airbrush":
			tools.airbrushRad = parseInt(option[1],10)*1.5 + 3;
			
			break;	
		case "line":
			tools.lineWidth = parseInt(option[1]);
			break;
		case "figure":
			if( element.parentNode.id !== undefined ){
				var toolSelected = element.parentNode.id.replace('_options','');
				if(option[1]==1){
					tools[toolSelected].stroke = true;
					tools[toolSelected].fill = false;
				}else if(option[1]==2){
					tools[toolSelected].stroke = true;
					tools[toolSelected].fill = true;
				}else if(option[1]==3){
					tools[toolSelected].stroke = false;
					tools[toolSelected].fill = true;
				}
			}
			break;					
	}
}

//////////////////////////////
//		%COLOR EVENTS		//
//////////////////////////////
function changeColor(color,prim){
	var type = prim ? 'primColor' : 'secColor';
	if(color){
		tools[type] = color;	
		document.getElementById(type).style.background = color;
	}
}

//////////////////////////////////// END-[EVENT HANDLERS] ////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%MATRIX OPERATIONS								///
///																					///
///////////////////////////////////////////////////////////////////////////////////////	
function saveImage(){	
	if(savedImagesData.ind < savedImagesData.images.length-1){
		savedImagesData.images.splice(savedImagesData.ind+1,savedImagesData.images.length-1-savedImagesData.ind);
	}
	if ( savedImagesData.images.length === 5 ){
		savedImagesData.images.splice(0,1);
	}else{
		savedImagesData.ind++;
	}

	window.canvasImageData = ctx.getImageData(0,0,canvas.width,canvas.height);

	savedImagesData.images.push(window.canvasImageData);
	
	if ( savedImagesData.ind > 0)
		window.edit.enable('undo');
}
function stepBack(){

	if( savedImagesData.ind > 0 ){
		var zoom = window.tools.magnifier.zoomMag;
		savedImagesData.ind--;
		window.canvasImageData = savedImagesData.images[savedImagesData.ind];

		if ( canvas.width != window.canvasImageData.width || canvas.height != window.canvasImageData.height){
			canvas.width = offscreenCanvas.width = window.canvasImageData.width;
			canvas.height = offscreenCanvas.height = window.canvasImageData.height;
			window.canvasChangeBoxSize(window.canvasImageData.width*zoom,window.canvasImageData.height*zoom);
			window.canvasAttributes.update();
		}

		ctx.putImageData(savedImagesData.images[savedImagesData.ind],0,0);	

		if ( savedImagesData.ind === 0 ){
			window.edit.disable('undo');//
		}
		window.edit.enable('repeat');
	}
}
function stepForward(){
	if( savedImagesData.ind <  savedImagesData.images.length-1){
		var zoom = window.tools.magnifier.zoomMag;
		savedImagesData.ind++;
		window.canvasImageData = savedImagesData.images[savedImagesData.ind];

		if ( canvas.width != window.canvasImageData.width || canvas.height != window.canvasImageData.height){
			canvas.width = offscreenCanvas.width = window.canvasImageData.width;
			canvas.height = offscreenCanvas.height = window.canvasImageData.height;
			window.canvasChangeBoxSize(window.canvasImageData.width*zoom,window.canvasImageData.height*zoom);
			window.canvasAttributes.update();
		}

		ctx.putImageData(savedImagesData.images[savedImagesData.ind],0,0);	
		
		if ( savedImagesData.ind === savedImagesData.images.length-1 ){
			window.edit.disable('repeat');
		}
		window.edit.enable('undo');
	}
}

function paste(e){
	var e = e || window.events;
	var content = "";
	var type = "Text";

	if ( tools.selected === 9 && text.cursor.active ){
		//IE
		if ( window.clipboardData ){						
			content = window.clipboardData.getData(type);

		//WEBKIT	
		}else if ( e ) {												
    		content = e.clipboardData.getData(type);
    	}
    	text.pasteText(content);
    }	
    // PASTE the selected Image
    if ( tools.selected < 2 ){
    	if ( selectionFrame.active ){
    		selectionFrame.deactivate();
    	}
    	selectionFrame.pasteImage();
    }
    if ( e )
    	return window.preventDefault(e);
}

function copy(e){
	if ( tools.selected === 9 && text.cursor.active ){
			var type = "Text";
            var success = true;
            var content = window.text.content;
            
            if ( window.clipboardData && clipboardData.setData ){// IE
				window.clipboardData.setData(type,content);
			}
    }
    // Copy the selected Image
    if ( tools.selected < 2 && selectionFrame.active ){
    	selectionFrame.copyImage();
    	window.edit.enable('paste');
    }

    if ( e )
    	return window.preventDefault(e);
}


function cut(e){
	if ( tools.selected === 9 && text.cursor.active ){
		if ( window.clipboardData && clipboardData.setData ){// IE
			clipboardData.setData(type,content);
		}
	}

	 // Cut the selected Image
    if ( tools.selected < 2 && selectionFrame.active ){
    	selectionFrame.copyImage();
    	selectionFrame.cutted.imageData = ctx.createImageData(canvas.width,canvas.height);
    	selectionFrame.cutted.imageDataNoFill = ctx.createImageData(canvas.width,canvas.height);
    	selectionFrame.deactivate();
    }
    if ( e )
    	return window.preventDefault(e);
}

// NEW PAINT
function newPaint(){
	if ( window.savedImagesData.images.length > 1){
		window.windowMsg.show(1,'Save changes to '+window.filename+'?',['Yes','No','Cancel'],[function(){downloadImage();resetPaint();},resetPaint,0],true);
	}else{
		window.resetPaint();
	}	
}
//////////////////////////
		// RESET ALL PAINT
		function resetPaint(){
			window.ctx.fillStyle = "rgba(255,255,255,255)";
			window.ctx.fillRect(0,0,canvas.width,canvas.height);
			window.offscreenCtx.clearRect(0,0,canvas.width,canvas.height);
			window.savedImagesData.ind = -1;
			window.savedImagesData.images.length = 0;
			window.saveImage();
		}

// TO LOAD AN IMAGE
function loadPaint(){
	var file = document.getElementById('imageToUpload').files[0];
	if ( file.type.indexOf("image") !== -1 ){
		window.canvasAttributes.changeFileName(file.name);
		window.loadImg.src = window.url.createObjectURL(file);
	}else{
		window.windowMsg.show(0,'The file can not be opened',['OK'],[0],false);
	}
}	

		//Get into CANVAS
		function loadIntoCanvas(e){
			window.canvasAttributes.change(window.loadImg.width,window.loadImg.height);
			window.offscreenCanvas.width = window.loadImg.width;
			window.offscreenCanvas.height = window.loadImg.height;		
			window.resetPaint();
			window.ctx.drawImage(window.loadImg,0,0);
			window.canvasImageData = window.ctx.getImageData(0,0,window.canvas.width,window.canvas.height);
			window.savedImagesData.images[0] = window.canvasImageData;
		}	

// TO SAVE DE CANVAS IMAGE INTO THE LOCAL COMPUTER
function downloadImage(){	
	try{
		var btn = document.getElementById('save_btn');
		btn.href = window.canvas.toDataURL('image/png');
		btn.download = window.filename+'.png';
		btn.click();	
	}catch(error){
		var msg = 'An error has occurred saving the image.<br/> You can save de image from Bitmap view (ctrl+F),<br/>doing right click -> Save image/picture as...';
		window.windowMsg.show(0,msg,['OK'],[0],false);
	}	
}


// DROP AN IMAGE OVER CANVAS
function dropImage(e){
	var files = e.dataTransfer.files;
	document.getElementById('app_blocker').style.display = 'block';

	if ( files.length > 0 ){
		var file = files[0];

		if ( typeof FileReader !== "undefined" && file.type.indexOf("image") !== -1 ){
			var reader = new FileReader();

			reader.onload = function (e){
				var elem = trigger(e);;
				window.dragImg.src = elem.result;
			}
			reader.readAsDataURL(file);
		}else{
			window.windowMsg.show(0,'The file can not be opened',['OK'],[0],false);
		}
	}
	return window.preventDefault(e);
}	

function dropIntoCanvas(e){
	var width = window.dragImg.width;
	var height = window.dragImg.height;

	if ( width > canvas.width || height > canvas.height ){
			window.windowMsg.show(1,
								'Do you want to enlarge the image?',
								['Yes','No','Cancel'],
								[function(){
									window.canvasAttributes.change(window.dragImg.width+10,window.dragImg.height+10);
									window.dropIntoCanvas();
								},
								window.selectionFrame.dragImage,
								function(){
									document.getElementById('app_blocker').style.display = 'none';
									window.dragImg.src='';
								}],false);
	}else{
		window.selectionFrame.dragImage();
	}

}
//////////////////////////////////// END-[MATRIX OPERATIONS] ////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%MENU OPERATIONS								///
///																					///
///////////////////////////////////////////////////////////////////////////////////////
var menu = {
	element:{},
	active : false,
	down : null,
	activation:function(elem){
		this.active = !this.active;
		if ( this.active ){
			this.element.className += ' active';
		}else{	
			window.menu.element.className = this.element.className.replace(' active','');
			this.hideShownsubmenu();
		}
		showSubmenu(elem);
	},
	showSubmenu: function(){
		if ( window.menu.down ){
			window.menu.down.style.display = 'block';
			document.getElementById('app_blocker').style.zIndex = '500';
			document.getElementById('app_blocker').style.display = 'block';
		}
	},
	hideShownsubmenu:function(){
		if ( window.menu.down ){
			window.menu.down.style.display = 'none';
			document.getElementById('app_blocker').style.zIndex = '900';
			if ( !window.windowMsg.shown )
				document.getElementById('app_blocker').style.display = 'none';	
		}
	}
}
function menuMouseDown(e){
	var element = trigger(e);
	window.menu.activation(element); 
	return window.preventDefault(e);
}

function menuMouseOver(e){
	var element = trigger(e);
	if ( window.menu.active ){
		showSubmenu(element);
	}
	return window.preventDefault(e);
}

function showSubmenu(element){
	if ( window.menu.active ){
		if (element.nodeName === 'SPAN'){
			element = element.parentNode;
		}
		if (element.nodeName === 'LI'){
			var submenu = document.getElementById(element.id+'_menu_container');
			if ( submenu !== null && submenu.style.display === 'none' ){
				
				if ( window.menu.down !== null && window.menu.down !== submenu ){
					 window.menu.hideShownsubmenu();
				}
				window.menu.down = submenu;
				window.menu.showSubmenu();
			}	
		}
	}else if ( window.menu.down !== null && window.menu.down.style === 'block' ){
		 window.menu.down.hideShownsubmenu();
	}
}

function submenuHandler(e){
	var element = trigger(e);
	
	/////////////////////////////////////////////////////
	while( element !== undefined && element !== null){
		if ( element.nodeName === 'TR' || element.nodeName === 'LI' ){
			break;
		}
		element = element.parentNode;
	}

	/////////////////////////////////////////////////////
	if ( element !== undefined && element !== null ){
		if ( element.className !== 'disabled' ){
			var actionName = element.id.split('_');
			//FILE MENU
			if ( actionName[0] === 'file' ){
				//NEW
					if( actionName[1] === 'new' ){
						window.newPaint();
					}
				//OPEN
					if ( actionName[1] === 'open' ){
						document.getElementById('imageToUpload').click();
					}	
				//SAVE
					if ( actionName[1] === 'save' ){
						window.downloadImage();
					}	
				//PRINT
					if ( actionName[1] === 'print' ){
						window.print();
					}	
				//SHARE
					if ( actionName[1] === 'share' ){
						document.getElementById('share_container').style.display = 'block';
					}		
			}
			//EDIT MENU
			if ( actionName[0] === 'edit' || actionName[0] === 'ctx' ){
				//UNDO
					if ( actionName[1] === 'undo'){
						window.stepBack();
					}
				//REPEAT
					if ( actionName[1] === 'repeat'){
						window.stepForward();
					}
				//CUT
					if ( actionName[1] === 'cut'){
						window.cut();
					}
				//COPY
					if ( actionName[1] === 'copy'){
						window.copy();
					}
				//PASTE
					if ( actionName[1] === 'paste'){
						window.paste();
					}
				//CLEAR SELECTION
					if ( actionName[1] === 'clear'){
						window.clearSelect();
					}
				//SELECT ALL
					if ( actionName[1] === 'selectall'){
						if ( window.tools.selected === 9 && window.text.cursor.active ){
							window.text.selectAll();
						}else {
							window.selectionFrame.selectAll();
						}	
					}	
				//PASTE FROM
					if ( actionName[1] === 'pastefrom'){
						
					}		
			}
			//VIEW MENU
			if ( actionName[0] === 'view' ){	
				//TOOLS
					if ( actionName[1] === 'left' ){
						window.showHide(actionName[0],actionName[1]);
					}
				//COLORS
					if ( actionName[1] === 'colors' ){
						window.showHide(actionName[0],actionName[1]);
					}	
				//STATUS
					if ( actionName[1] === 'footer' ){
						window.showHide(actionName[0],actionName[1]);
					}								
				//ZOOM
					if ( actionName[1] === 'zoom' ){
					}
				//TEXT
					if ( actionName[1] === 'text' ){
						if (element.className.indexOf('disabled') === (-1) ){
							window.textTool.changeShown();
						}
					}	
				//BITMAP
					if ( actionName[1] === 'bitmap' ){
						window.bitmap.showToggle();
					}	
			}
			//IMAGE MENU
			if ( actionName[0] === 'image'  || actionName[0] === 'ctx' ){
				//FLIP&ROTATE
					if ( actionName[1] === 'flip'){
						window.flipRotate.showToggle();
					}
				//STRETCH&SKEW
					if ( actionName[1] === 'stretch'){
						window.stretchSkew.showToggle();
					}	
				//INVERSE COLORS
					if ( actionName[1] === 'invert'){
						window.invertImageColor();
					}
				//ATTRIBUTES
					if ( actionName[1] === 'attributes'){
						window.canvasAttributes.show();
					}	
				//CLEARIMAGE
					if ( actionName[1] === 'clearimage'){
						window.tools.clearImage.clear();
					}
				//OPAQUE
					if ( actionName[1] === 'opaque' ){
						var elemId = tools.backgroundTransp ? 'fill_option2' : 'fill_option1';
						var element = document.getElementById(elemId);
						optionClasesChanger(element);
						window.tools.changeBackgroundTransp();
				//
					}
			}
			//COLOR MENU
			if ( actionName[0] === 'color'  && actionName[1] === 'edit' ){
				window.editColors.showToggle();
			}
			//HELP MENU
			if ( actionName[0] === 'help' ){
				//ABOUT
					if ( actionName[1] === 'about'){
						document.getElementById('about_container').style.display = 'block';
					}
			}	
			if ( actionName[0] === 'ctx' ){
				window.ctxMenu.hide();
			}else{	
				window.menu.hideShownsubmenu();
			}	
		}	
	}
	return window.preventDefault(e);	
}

// SHOW TOGGLE OF TOOLS BLOCKS
function showHide(menu,name){
	var element = document.getElementById(name+'_container');
	var lineCheck = get_firstchild(document.getElementById(menu+'_'+name));
	if ( element ){
		if ( element.style.display === 'block' ){
			element.style.display = 'none';
			lineCheck.classList.remove('check');
		}else{
			element.style.display = 'block';
			lineCheck.classList.add('check');
		}
	}
	window.sizeApp();
}

//////////////////////////////////// END-[MENU OPERATIONS] ////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
///																					///
///									%INITIALIZE										///
///																					///
///////////////////////////////////////////////////////////////////////////////////////	

function sizeApp(){
	var appWin = document.getElementById("canvas_container");
	var mainHeader = document.getElementById('main_header').getBoundingClientRect();
	var menuBlock = document.getElementById('menu_container').getBoundingClientRect();
	var colorBlock = document.getElementById('colors_container').getBoundingClientRect();
	var statusBlock = document.getElementById('footer_container').getBoundingClientRect();
	var toolsBlock = document.getElementById('left_container').getBoundingClientRect();
	var screenDim = window.viewPortDim();

	// CALCULATING CANVAS CONTAINER NEW SIZE
	canvasBlockHeight = screenDim.height - mainHeader.height - menuBlock.height - colorBlock.height - statusBlock.height - 15;
	canvasBlockWidth = screenDim.width - toolsBlock.width -60;

	// RESIZING CANVAS CONTAINER
	appWin.style.height = canvasBlockHeight.toString()+"px";
	appWin.style.width = canvasBlockWidth.toString()+"px";

	// MAGNIFIER MATHS
		window.tools.magnifier.appSize.width = canvasBlockWidth;	
		window.tools.magnifier.appSize.height = canvasBlockHeight;
		window.tools.magnifier.calcZoomWin();

	// UPDATING CANVAS IMAGE DATA & SAVE IMAGES
		canvasImageData = window.ctx.getImageData(0,0,canvas.width,canvas.height);
		window.savedImagesData.images[window.savedImagesData.ind] = window.canvasImageData;	

	window.bitmap.update();	
}

function canvasChangeBoxSize(w,h){
	canvas.style.width = w.toString()+"px";
	canvas.style.height = h.toString()+"px";
	document.getElementById('dim_frame').style.width = (w+6).toString()+'px';
	document.getElementById('eDim').style.marginTop = (h/2 -2).toString()+'px';
}


//////////////////////////////////////////////////////////////////////////////////////
//	OBJECT - resizeCanvas 															//
//		Manage the resizing of the canvas size, this is the number of pixels,		//
//		from the resize point (blue squares) in the margin of canvas.				//
//		-	takeCoords(e): Initialization of the necesary elements for the resize	//
// 						maths.														//
//		-	move(e): Take care of marker effect while mouse is pressed in the 		//
//						movement, to user see how long is canvas going to be.		//
//		-	redimension(e): Resize canvas according to last marker size, which		//
//						is hidden. Some functions are called to update the 			//
//						current cavas image data and saved image data.				//
//////////////////////////////////////////////////////////////////////////////////////
var resizeCanvas = {
	origin:{x:0,y:0,w:0,h:0},
	active:false,
	marker:null,
	mover:'',
	takeCoords: function(e){
		var coord = mouseCoord(e);
		var rC = window.resizeCanvas;
		// INITIALIZATION OF THE MARKER AND THE REST OF THIS OBJECT
		rC.active = true;
		rC.origin.x = coord.x;
		rC.origin.y = coord.y;
		rC.mover = trigger(e).id;
		rC.origin.w = parseInt(canvas.style.width.replace('px',''));
		rC.origin.h = parseInt(canvas.style.height.replace('px',''));
		rC.marker.style.width = canvas.style.width;
		rC.marker.style.height = canvas.style.height;

		rC.marker.style.display = 'block';

		return window.preventDefault(e);
	},
	redimension: function(e){
		var coord = window.mouseCoord(e);
		var rC = window.resizeCanvas;
		var deltaX = rC.origin.w + (coord.x - rC.origin.x); 
		var deltaY = rC.origin.h + (coord.y - rC.origin.y);
		// RESET THIS OBJECT PARAMETERS
		rC.active = false;
		rC.marker.style.display = 'none';

		// CHANGE THE CANVAS PIXEL SIZE
			window.canvasAttributes.change(Math.round(deltaX/window.tools.magnifier.zoomMag),Math.round(deltaY/window.tools.magnifier.zoomMag));
	},
	move: function(e){
		var coord = mouseCoord(e);
		var rC = window.resizeCanvas;
		var deltaX = rC.origin.w + (coord.x - rC.origin.x); 
		var deltaY = rC.origin.h + (coord.y - rC.origin.y);
		// UPDATING DIMENSIONS OF THE MARKER
			if ( rC.mover === 'eDim' ){		
				rC.marker.style.width = deltaX.toString() + "px";
			}
			if ( rC.mover === 'seDim' ){		
				rC.marker.style.width = deltaX.toString() + "px";
				rC.marker.style.height = deltaY.toString() + "px";
			}	
			if ( rC.mover === 'sDim' ){		
				rC.marker.style.height = deltaY.toString() + "px";
			}
		return window.preventDefault(e);	
	}
}


// ADDING A DASH LINE METHOD TO THE CONTEXT
CanvasRenderingContext2D.prototype.dashLine = {dashLength:5,lineWidth:1,lineColor:'rgb(0,0,168)'};
CanvasRenderingContext2D.prototype.drawDashedRect = function(x1,y1,x2,y2){
	
	var deltaX = x2 - x1;
	var deltaY = y2 - y1;
	var numDashes = Math.floor(Math.sqrt(deltaX*deltaX + deltaY*deltaY)/this.dashLine.dashLength);
	
	this.save();
		this.lineWidth = this.dashLine.lineWidth;

		this.strokeStyle = '#FFFFFF';
		this.beginPath();
		this.moveTo(x1,y1);
		this.lineTo(x2,y2);
		this.stroke();

		this.strokeStyle = this.dashLine.lineColor;
		this.beginPath();
		for(var i=0; i<numDashes; ++i){
				this[i%2 === 0 ? 'moveTo' : 'lineTo']
				(x1 + i*deltaX/numDashes, y1 + i*deltaY/numDashes);
		}		
		this.stroke();
	
	this.restore();
}

function inicializa(){
		//////////////////////////////////////////////
		//					INITS 					//
		//////////////////////////////////////////////
		//APP CONFIG
			window.sounds.chord.src = 'sounds/chord.mp3';
			window.sounds.error.src = 'sounds/error.mp3';
			window.bitmap.element = document.getElementById('bitmap_container');
			window.bitmap.img = document.getElementById('bitmap');
			addEvent(window.bitmap.element,'mousedown',window.bitmap.showToggle,false);
		//Tools
			tools.clearImage.menuElement = document.getElementById('image_clearimage');
			addEvent(document.getElementById('eDim'),'mousedown',resizeCanvas.takeCoords,false);
			addEvent(document.getElementById('seDim'),'mousedown',resizeCanvas.takeCoords,false);
			addEvent(document.getElementById('sDim'),'mousedown',resizeCanvas.takeCoords,false);

		//Messages Window
			window.windowMsg.windowElement = document.getElementById('confirm_window');
			window.windowMsg.msgElement = document.getElementById('info_message');
			window.windowMsg.iconElement = document.getElementById('info_icon');
			window.windowMsg.closeBtn = document.getElementById("confirm_close");
			window.windowMsg.btns[0] = document.getElementById("confirm_frame_btn_0");
			window.windowMsg.btns[1] = document.getElementById("confirm_frame_btn_1");
			window.windowMsg.btns[2] = document.getElementById("confirm_frame_btn_2");

			addEvent(window.windowMsg.closeBtn,'mouseup',function(){window.windowMsg.hide(-1)},false);
			addEvent(window.windowMsg.btns[0],'mouseup',function(){window.windowMsg.hide(0)},false);
			addEvent(window.windowMsg.btns[1],'mouseup',function(){window.windowMsg.hide(1)},false);
			addEvent(window.windowMsg.btns[2],'mouseup',function(){window.windowMsg.hide(2)},false);

		//CANVAS & CONTEXT
			//Atributes
				window.canvasAttributes.element = document.getElementById('attributes_container');
				window.canvasAttributes.widthEl = document.getElementById('attr_width');
				window.canvasAttributes.heightEl = document.getElementById('attr_height');
			canvas = document.getElementById("lienzo");
			offscreenCanvas = document.createElement("canvas");
			
			canvas.width = 800;
			canvas.height = 500;
			window.canvasAttributes.update();
			offscreenCanvas.width = 800;
			offscreenCanvas.height = 500;	
			
			ctx = canvas.getContext("2d");
			offscreenCtx = offscreenCanvas.getContext("2d");
			
			ctx.fillStyle = "rgba(255,255,255,255)";
			ctx.fillRect(0,0,canvas.width,canvas.height);
			// SMOOTH
				ctx.webkitImageSmoothingEnabled = false;
				ctx.mozImageSmoothingEnabled = false;
				ctx.msImageSmoothingEnabled = false;
				ctx.imageSmoothingEnabled = false; /// future
				ctx.lineCup = 'butt';

				offscreenCanvas.webkitImageSmoothingEnabled = false;
				offscreenCanvas.mozImageSmoothingEnabled = false;
				offscreenCanvas.msImageSmoothingEnabled = false;
				offscreenCanvas.imageSmoothingEnabled = false; /// future
				offscreenCtx.lineCup = 'butt';	

				if ( ctx.patternQuality ){
					ctx.patternQuality = "fast";
					offscreenCanvas.patternQuality = "fast";
				}
			
			//SIZE ADAPTATION FOR DIFFERENT PIXEL RATIOS
				//http://www.html5rocks.com/en/tutorials/canvas/hidpi/
			var devicePixelRatio = window.devicePixelRatio || 1;
        	var backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
                            ctx.mozBackingStorePixelRatio ||
                            ctx.msBackingStorePixelRatio ||
                            ctx.oBackingStorePixelRatio ||
                            ctx.backingStorePixelRatio || 1;
			var ratio = devicePixelRatio / backingStoreRatio;

			if ( ratio != 1 ){
				ctx.setTransform(ratio, 0, 0, ratio, 0, 0);
				offscreenCtx.setTransform(ratio, 0, 0, ratio, 0, 0);
			}

			changeColor("rgb(0,0,0)",true);
			changeColor("rgb(255,255,255)",false);

		// Background pattern for the size dummy on SelectionFrame
			var imgPat01 = new Image();
			var imgPat02 = new Image();
			imgPat01.onload = function(){
				window.selectionFrame.sizeDummy.fillPat.push(window.offscreenCtx.createPattern(imgPat01,'repeat'));
			};
			imgPat02.onload = function(){
				window.selectionFrame.sizeDummy.fillPat.push(window.offscreenCtx.createPattern(imgPat02,'repeat'));
			};
			imgPat01.src = 'images/select_backgnd.gif';
			imgPat02.src = 'images/get_size.gif';

		//////////////////////////////////////////////
		//					EVENTS 					//
		//////////////////////////////////////////////
			menu.element = document.getElementById("menu_container");
			window.resizeCanvas.marker = document.getElementById("resize_canvas_marker");
			addEvent(document.getElementById('imageToUpload'),'change',window.loadPaint,false);
			addEvent(window.loadImg,'load',window.loadIntoCanvas,false);

		//GENERAL EVENTS
			addEvent(document.getElementById('app_blocker'),'mousedown',window.menu.hideShownsubmenu,false);
			addEvent(menu.element,'mousedown',menuMouseDown,false);
			addEvent(menu.element,'mouseover',menuMouseOver,false);
			addEvent(document.getElementById('submenu_container'),'mousedown',submenuHandler,false);
		//CONTEXT Menu
			ctxMenu.element = document.getElementById("ctx_menu_container");
			addEvent(ctxMenu.element,'mousedown',submenuHandler,false);
			addEvent(document,'contextmenu',ctxMenu.eventshot,false);
		// TOOL BUTTONS events	
			addEvent(document.getElementById("tools_container"),'mousedown',takeToolSelected,false);
		// TOOL OPTIONS
			addEvent(document.getElementById("options_container"),'mousedown',selectOption,false);
		// TEXT TOOLBAR
			window.textTool.menuElement = document.getElementById('view_text');
			window.textTool.toolbar.element = document.getElementById('text_container');
			window.textTool.toolbar.header = document.getElementById('text_header');
			window.textTool.font.listElement = document.getElementById('font_list');
			window.textTool.size.listElement = document.getElementById('size_list');
			window.textTool.size.inputText = document.getElementById('input_text_size');
			window.textTool.font.inputText = document.getElementById('selected_font_item');

			window.textTool.updateFont();

			addEvent(window.textTool.toolbar.element,'mousedown',window.textTool.mousedownToolbar,false);
			addEvent(window.textTool.size.inputText,'blur',window.textTool.handleTextSize,false);
			addEvent(window.textTool.size.listElement,'mousedown',window.textTool.handleSizeList,false);
			addEvent(window.textTool.font.listElement,'mousedown',window.textTool.handleFontList,false);
		// FLIP & ROTATE WINDOW
			window.flipRotate.winEl = document.getElementById('flip_rotate_container');
			window.flipRotate.okBtn = document.getElementById('fliprotate_ok');
			window.flipRotate.cancelBtn = document.getElementById('fliprotate_cancel');
			window.flipRotate.closeBtn = document.getElementById('fliprotate_close');

			addEvent(window.flipRotate.closeBtn,'mouseup',window.flipRotate.showToggle,false);
			addEvent(window.flipRotate.cancelBtn,'mouseup',window.flipRotate.showToggle,false);
			addEvent(window.flipRotate.okBtn,'mouseup',window.flipRotate.action,false);
			addEvent(document.getElementById('fliprotate_options'),'mousedown',window.flipRotate.optSelection,false);
		// ATTRIBUTES WINDOW
			addEvent(document.getElementById('attributes_close'),'mouseup',window.canvasAttributes.hide,false);
			addEvent(document.getElementById('attribute_cancel'),'mouseup',window.canvasAttributes.hide,false);
			addEvent(document.getElementById('attribute_ok'),'mouseup',function(){window.canvasAttributes.change();},false);
		// DEFINE COLORS WINDOW
			addEvent(document.getElementById('custom_color_container'),'mousedown',function(e){window.editColors.selectColorBox(e,'custom');},false);
			addEvent(document.getElementById('basic_color_container'),'mousedown',function(e){window.editColors.selectColorBox(e,'basic');},false);
			window.editColors.HSLSelEL.h =  document.getElementById('defined_hue');
			window.editColors.HSLSelEL.s =  document.getElementById('defined_sat');
			window.editColors.HSLSelEL.l =  document.getElementById('defined_lum');
			window.editColors.RGBSelEl.r =  document.getElementById('defined_red');
			window.editColors.RGBSelEl.g =  document.getElementById('defined_green');
			window.editColors.RGBSelEl.b =  document.getElementById('defined_blue');
			window.editColors.clrBoxSel.basic = document.getElementById('bc40');
			window.editColors.clrBoxSel.custom = document.getElementById('cc0');
			window.editColors.clrBrushSel.element = document.getElementById('0');

			addEvent(window.editColors.HSLSelEL.h,'change',window.editColors.takeHSL,false);
			addEvent(window.editColors.HSLSelEL.s,'change',window.editColors.takeHSL,false);
			addEvent(window.editColors.HSLSelEL.l,'change',window.editColors.takeHSL,false);
			addEvent(window.editColors.RGBSelEl.r,'change',window.editColors.takeRGB,false);
			addEvent(window.editColors.RGBSelEl.g,'change',window.editColors.takeRGB,false);
			addEvent(window.editColors.RGBSelEl.b,'change',window.editColors.takeRGB,false);

			window.editColors.lumiPointer =  document.getElementById('lumi_pointer');
			window.editColors.colorSelEl =  document.getElementById('picked_color');
			window.editColors.element = document.getElementById('editcolors_container');
			window.editColors.colorCanvas = document.getElementById('define_color_canvas');
			window.editColors.colorCtx = window.editColors.colorCanvas.getContext('2d');
			window.editColors.colorTable.src = "images/color_table.png";
			addEvent(document.getElementById('lumibar_container'),'mousedown',function(e){window.editColors.lumiPress=true;window.editColors.takeLumi(e);},false)
			addEvent(document.getElementById('lumibar_container'),'mousemove',window.editColors.takeLumi,false);
			addEvent(window.editColors.colorCanvas,'mousedown',function(e){window.editColors.mousepress=true;window.editColors.takeColor(e);},false);
			addEvent(window.editColors.colorCanvas,'mousemove',window.editColors.takeColor,false);
			addEvent(document.getElementById('editcolor_close'),'mouseup',window.editColors.showToggle,false);
			addEvent(document.getElementById('color_cancel'),'mouseup',window.editColors.showToggle,false);
			addEvent(document.getElementById('color_go'),'mouseup',window.editColors.action,false);
			addEvent(document.getElementById('add_color_define'),'mouseup',window.editColors.addColor,false);
			addEvent(document.getElementById('color_define'),'mouseup',window.editColors.showDefine,false);
		// SKEW & STRETCH
			window.stretchSkew.elementWin = document.getElementById('skew_container');
			window.stretchSkew.stretchH = document.getElementById('h_stretch_value');
			window.stretchSkew.stretchV = document.getElementById('v_stretch_value');
			window.stretchSkew.skewH = document.getElementById('h_skew_value');
			window.stretchSkew.skewV = document.getElementById('v_skew_value');

			addEvent(document.getElementById('skew_close'),'mouseup',window.stretchSkew.showToggle,false);
			addEvent(document.getElementById('skew_cancel'),'mouseup',window.stretchSkew.showToggle,false);
			addEvent(document.getElementById('skew_ok'),'mouseup',window.stretchSkew.action,false);	
		// SHARE WINDOW
			addEvent(document.getElementById('share_close'),'mouseup',function(e){ document.getElementById('share_container').style.display='none'},false);
		// ABOUT WINDOW
			var aboutWin = document.getElementById('about_container');
			addEvent(document.getElementById('about_close'),'mouseup',function(e){aboutWin.style.display='none'},false);
			addEvent(document.getElementById('about_go'),'mouseup',function(e){aboutWin.style.display='none'},false);
		// CANVAS events

			// DRAG & DROP Image
			addEvent(window,'dragover',preventDefault,false);
			addEvent(canvas,'drop',window.dropImage,false);
			addEvent(window.dragImg,'load',window.dropIntoCanvas,false);

			// Mouse
			addEvent(canvas,'mouseover',mouseOver,false);
			addEvent(document,'mousemove',coordUpdate,false);
			addEvent(canvas,'mouseout',mouseOut,false);		
			addEvent(canvas,'mousedown',clickDownCanvas,false);								
			addEvent(document,'mouseup',clickUpCanvas,false);
		// COLOR events
			addEvent(document.getElementById("colors"),'mousedown',function(e){
							var element = trigger(e);
							canvasMouse.right = rightBtn(e);
							if ( element.id.length < 3 && !canvasMouse.right){
								changeColor(element.style.backgroundColor,!canvasMouse.right);
								window.editColors.clrBrushSel.rightBtn = false;	
								window.editColors.clrBrushSel.element = element;
							}
			},false);	
			addEvent(document.getElementById("colors"),'dblclick',function(e){
							var element = trigger(e);
							if ( element.id.length < 3 && !canvasMouse.right){
								window.editColors.showToggle();
							}
			},false);

		//	KEY EVENTS
			addEvent(document,'keydown',keyDown,false);
			addEvent(document,'keypress',keyPress,false);	
			addEvent(document,'keyup',keyUp,false);

			addEvent(document,'paste',paste,false);	
			addEvent(document,'copy',copy,false);	
			addEvent(document,'cut',cut,false);	

		//WINDOWS DRAG EVENTS
			var windows_headers = document.getElementsByClassName('header');
			for ( var i=0,end=windows_headers.length; i<end;i++ ){
				if ( windows_headers[i].nodeType == 1 && windows_headers[i].nodeName === 'DIV' ){
					addEvent(windows_headers[i],'mousedown',window.windowsDragging.mousedown,false);
				}
			}

		//TITLE EVENTS
			var customTitle = new CustomTitle(window.showElTitle,window.hideElTitle);

		// CONFIG
			sizeApp();
			saveImage();	

		// COOKIES
			if ( window.navigator.cookieEnabled ){
				var cookiesMsg = 'This site uses cookies to store<br/> information on your computer';
				window.windowMsg.show(1,cookiesMsg,['OK'],[0],false);
			}
	}

	///////////////// INIT LAUNCHER /////////////////	
		addEvent(window,'load',window.inicializa,false);
	/////////////////////////////////////////////////
		addEvent(window,'resize',window.sizeApp,false);
/************************************************************************/